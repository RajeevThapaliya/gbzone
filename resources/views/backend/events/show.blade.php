@extends('backend/app')
@section('content')
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Event Detail</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
        <div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
          <!-- <div class="panel-heading font-bold">
            Create New Zone
          </div> -->
          {!! Form::hidden('latitude',$event_info[0]->latitude,['id'=>'latitude']) !!}
            {!! Form::hidden('longitude',$event_info[0]->longitude,['id'=>'longitude']) !!}
          <div class="panel-body">
            <div class="row">
            <?php ?>
            <table>
              <tr>
                  <td>Image</td>                   
                  <td><img src="{{ asset('uploads/events/'.$event_info[0]->image)}}" style="height: 100px; margin-top: 10px;" /></td>
               </tr>
              <tr>
                  <td>Type</td>                   
                  <td>{{$event_info[0]->type}}</td>
               </tr>
               <tr>
                  <td>City</td>                   
                  <td>{{$event_info[0]->city}}</td>
               </tr>
               @if($event_info[0]->type=='general')
               <tr>
                  <td>Start Date</td>                   
                  <td>{{$event_info[0]->start_date}}</td>
               </tr>
               <tr>
                  <td>End Date</td>                   
                  <td>{{$event_info[0]->end_date}}</td>
               </tr>
               @endif
                <tr>
                  <td>Description</td>                   
                  <td>{{$event_info[0]->description}}</b></td>
               </tr>
              </table>
            </div>
         </div>
      </div>
      <div class="form-group">
           <div id="display_map" style="height:500px"></div>             
      </div>      
</div>
</div>
</div>
</div>
@endsection