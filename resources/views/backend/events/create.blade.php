@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Create New Event</h1>
			</div>
			<div class="wrapper-md" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-12 col-xs-12">
				<div class="panel panel-default">
					<!-- <div class="panel-heading font-bold">
						Create New Event
					</div> -->
					<div class="panel-body">
						<!-- @if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif -->
						{!! Form::open(['route' => 'admin.events.store','data-parsley-validate','files'=>true]) !!}
						{!! Form::hidden('latitude','27.7172453',['id'=>'latitude']) !!}
						{!! Form::hidden('longitude','85.3239605',['id'=>'longitude']) !!}
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('name', 'Title:', ['class' => 'control-label']) !!}
									{!! Form::text('name', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('type', 'Type:', ['class' => 'control-label']) !!}
									{!! Form::select('type',array(null=>'Please Select')+$event_type,null,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select type')) !!}


									@if ($errors->has('type')) <p class="help-block">{{ $errors->first('type') }}</p> @endif
								</div>
								
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('start_date', 'Start Date:', ['class' => 'control-label']) !!}
									{!! Form::text('start_date', null, ['class' => 'form-control datepick required','data-trigger'=>'change focusout','id'=>'start_date', 'readonly'=>'readonly']) !!}

									@if ($errors->has('start_date')) <p class="help-block">{{ $errors->first('start_date') }}</p> @endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('end_date', 'End Date:', ['class' => 'control-label']) !!}
									{!! Form::text('end_date', null, ['class' => 'form-control datepick required','data-trigger'=>'change focusout','id'=>'end_date', 'readonly'=>'readonly']) !!}

									@if ($errors->has('end_date')) <p class="help-block">{{ $errors->first('end_date') }}</p> @endif
								</div>
							</div>							
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('location', 'Location:', ['class' => 'control-label']) !!}
									{!! Form::text('location', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('location')) <p class="help-block">{{ $errors->first('location') }}</p> @endif
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
									{!! Form::text('city', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
								</div>								
							</div>
						</div>

						

						<div class="row">
							<div class="col-md-6">								
								<div class="form-group">
									{!! Form::label('invite_to', 'Invite To:', ['class' => 'control-label']) !!}
																	
									@foreach($invite_to as $k => $v)
										<div class="radio">
							              <label class="i-checks i-checks-sm">
							                <input type="radio" name="invite_to" value="{{$k}}">
							                <i></i>
							               {{$v}}
							              </label>
							            </div>										
									@endforeach
									@if ($errors->has('invite_to')) <p class="help-block">{{ $errors->first('invite_to') }}</p> @endif
								</div>

								<div class="form-group">
									{!! Form::label('image', 'Image:', ['class' => 'control-label']) !!}
									<!-- {!! Form::file('image', null, ['class' => 'form-control']) !!} -->
									<div class="image_select">
										<input type="file" name="image" id="file-1" class="inputfile inputfile-6 required" data-trigger="change focusout" data-multiple-caption="{count} files selected" multiple />
										<label for="file-1"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a Image&hellip;</strong></label>
									</div>										
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
									{!! Form::textarea('description', null, ['class' => 'form-control required','data-trigger'=>'change focusout','rows'=>7]) !!}

									@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
								</div>
							</div>
						</div>

						<div class="form-group">
							<input id='search_address' name="search_address" class='form-control' type='text' value='kathmandu, Nepal' placeholder='Search Box'>
							<div id="display_map" style="height:500px"></div>							
						</div>
						
						<button type="submit" name="save_event" value="save_event" class="btn btn-sm btn-primary">Submit</button>
						<button type="submit" name="save_notify" value="save_and_notify" class="btn btn-sm btn-primary">Submit And Push</button>
						<button type="reset" class="btn btn-sm btn-primary" onclick="window.location.href='<?php echo URL::to('admin/events');?>'"">Cancel</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
