@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Events</h1>
		</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{url('admin/events/create')}}" class="btn btn-sm btn-primary btn-addon"><i class="fa fa-plus"></i>Add New Event</a>
				</div>
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">SN</th>
								<th style="width:25%">Name</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Location</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if($events)
							<?php $i = 1; ?>
							@foreach($events as $event)
								<tr>
									<td>{{$i}}</td>
									<td>{{$event->name}}</td>
									<td>{{$event->start_date}}</td>
									<td>{{$event->end_date}}</td>
									<td>{{$event->location}}</td>
									<td>
										<a href="{{ route('admin.events.show',$event->id) }}" class="view btn btn-success btn-single" data-toggle="tooltip" title="Detail"><i class="fa fa-file-text-o"></i></a>
										<a class="edit btn btn-success btn-single" data-toggle="tooltip" title="Edit" href="{{ route('admin.events.edit', $event->id) }}"><i class="fa fa-edit"></i></a>
										{!! Form::open(['method' => 'DELETE', 'route'=>['admin.events.destroy', $event->id],'onsubmit' => 'return confirm("Do you want to delete this event?")']) !!}
										<!-- {!!Form::button('<i class="icon icon-trash"></i> ', array('type' => 'submit')); !!} -->
										{!!Form::button('<i class="icon icon-trash"></i> ', array(
									           'type' => 'submit',
									           'title' => 'Delete',
									           'data-toggle' => 'tooltip',
									           'class'=> 'btn btn-success btn-single',
									           'onclick'=>'return confirm("Are you sure?")'
									   	)); !!}
										{!! Form::close() !!}
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@else
							No events added yet.
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection