<header id="header" class="app-header navbar" role="menu">
	<!-- navbar header -->
	<div class="navbar-header bg-dark">
		<button class="pull-right visible-xs dk" ui-toggle="show" target=".navbar-collapse">
			<i class="glyphicon glyphicon-cog"></i>
		</button>
		<button class="pull-right visible-xs" ui-toggle="off-screen" target=".app-aside" ui-scroll="app">
			<i class="glyphicon glyphicon-align-justify"></i>
		</button>
		<!-- brand -->
		<a href="#/" class="navbar-brand text-lt">
			<!-- <i class="fa fa-btc"></i> -->
			<img src="{{ asset('backend/img/logo.png')}}" alt="." class="hide">
			<span class="hidden-folded m-l-xs">GB APP</span>
		</a>
		<!-- / brand -->
	</div>
	<!-- / navbar header -->

	<!-- navbar collapse -->
	<div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
		<!-- buttons -->
		<div class="nav navbar-nav hidden-xs">
			<a href="#" class="btn no-shadow navbar-btn" ui-toggle="app-aside-folded" target=".app">
				<i class="icon-list fa-fw text"></i>
				<i class="fa fa-indent fa-fw text-active"></i>
			</a>			
		</div>



			

			<div class="col-md-6" style="margin: auto;">
                    <!-- Meassage-->
                    @if(Session::has('error-message') || Session::has('success-message'))
                                
                        @if ($alert  = Session::get('error-message'))                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <strong>{{ $alert }}</strong>
                        </div>
                                                        
                        @elseif($alert = Session::get('success-message'))                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                            
                            <strong>{{ $alert }}</strong>
                        </div>
                            
                        @endif
                    @endif
                </div>

		<!-- link and dropdown -->
		<!-- search form -->

		<!-- nabar right -->
		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
				<a href="#" data-toggle="dropdown" class="dropdown-toggle clear">
					<span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
						<img src="{{ asset('backend/img/a0.jpg')}}" alt="...">
						<i class="on md b-white bottom"></i>
					</span>
					<span class="auth-name hidden-sm hidden-md">{{Auth::user()->name}}</span> <b class="caret"></b>
				</a>
				<!-- dropdown -->
				<ul class="dropdown-menu animated fadeInRight w">
					<li>
						<a href="{{ route('admin.changepassword') }}"><i class="fa fa-key"></i> Change Password</a>
					</li>
					<li>
						<a href="{{ url('/auth/logout') }}"><i class="fa fa-lock"></i> Logout</a>
					</li>
				</ul>
				<!-- / dropdown -->
			</li>
		</ul>
		<!-- / navbar right -->
	</div>
	<!-- / navbar collapse -->
</header>

<aside id="aside" class="app-aside hidden-xs bg-dark">
	<div class="aside-wrap">
		<div class="navi-wrap">
			<!-- user -->
			<div class="clearfix hidden-xs text-center hide" id="aside-user">
				<div class="dropdown wrapper">
					<!-- dropdown -->
					<ul class="dropdown-menu animated fadeInRight w hidden-folded">
						<li class="wrapper b-b m-b-sm bg-info m-t-n-xs">
							<span class="arrow top hidden-folded arrow-info"></span>
							<div>
								<p>300mb of 500mb used</p>
							</div>
							<div class="progress progress-xs m-b-none dker">
								<div class="progress-bar bg-white" data-toggle="tooltip" data-original-title="50%" style="width: 50%"></div>
							</div>
						</li>
						<li>
							<a href="">Settings</a>
						</li>
						<li>
							<a href="page_profile.html">Profile</a>
						</li>
						<li>
							<a href="">
								<span class="badge bg-danger pull-right">3</span>
								Notifications
							</a>
						</li>
					</ul>
					<!-- / dropdown -->
				</div>
				<div class="line dk hidden-folded"></div>
			</div>
			<!-- / user -->
			<!-- nav -->

			<nav ui-nav="" class="navi clearfix">
				<ul class="nav">
					<li>
						<a href="{{url().'/'}}">
							<i class="icon-screen-desktop icon text-info-lter"></i>
							<span class="font-bold">Home</span>
						</a>
					</li>
					<?php if(Auth::user()->type == 'admin'):?>
						<li>
							<a href="{{url('admin/brands')}}">
								<i class="icon-briefcase icon text-info-lter"></i>
								<span class="font-bold">Brands</span>
							</a>
						</li>
						<li>
							<a href="{{url('admin/zones')}}">
								<i class="icon-globe icon text-info-lter"></i>
								<span class="font-bold">Zones</span>
							</a>
						</li>
						<li>
							<a href="{{url('admin/partners')}}">
								<i class="icon-users icon text-info-lter"></i>
								<span class="font-bold">Partners</span>
							</a>
						</li>
						<li>
							<a href="{{url('admin/events')}}">
								<i class="icon-calendar icon text-info-lter"></i>
								<span class="font-bold">Events</span>
							</a>
						</li>
						<li>
							<a href="{{url('admin/offers')}}">
								<i class="icon-handbag icon text-info-lter"></i>
								<span class="font-bold">Offers</span>
							</a>
						</li>
						<li>
							<a href="{{url('admin/redeemeds')}}">
								<i class="icon-drawer text-info-lter"></i>
								<span class="font-bold">Redeemed Offers</span>
							</a>
						</li>
					<?php endif;?>
					<?php if(in_array(Auth::user()->type, array('partner'))):?>
						<li>
							<a href="{{url('admin/offers')}}">
								<i class="icon-handbag icon text-info-lter"></i>
								<span class="font-bold">My Offer</span>
							</a>
						</li>
						<li>
							<a href="{{url('admin/redeemeds')}}">
								<i class="icon-drawer text-info-lter"></i>
								<span class="font-bold">Redeemed Offers</span>
							</a>
						</li>
						<li class="line dk"></li>
					<?php endif;?>
				</ul>
		</nav>
		<!-- nav -->
		<!-- aside footer -->
		<!-- / aside footer -->
	</div>
</div>
</aside>
