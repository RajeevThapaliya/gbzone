@extends('backend/app')
@section('content')
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Offer Detail</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
        <div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
          <!-- <div class="panel-heading font-bold">
            Create New Zone
          </div> -->
          <div class="panel-body">
            <div class="row">
            <?php ?>
            <table>
              <tr>
                  <td>Image</td>                   
                  <td><img src="{{ asset('uploads/offers/'.$offer_info[0]->image)}}" style="height: 100px; margin-top: 10px;" /></td>
               </tr>

              <tr>
                  <td>Title</td>                   
                  <td>{{$offer_info[0]->title}}</td>
               </tr>
               <tr>
                  <td>Type</td>                   
                  <td>{{$offer_info[0]->type}}</td>
               </tr>
               @if($offer_info[0]->type=='general')
               <tr>
                  <td>Start Date</td>                   
                  <td>{{$offer_info[0]->start_date}}</td>
               </tr>
               <tr>
                  <td>End Date</td>                   
                  <td>{{$offer_info[0]->end_date}}</td>
               </tr>
               @endif
               @if(Auth::user()->type == 'admin')
               <tr>
                  <td>Offer Valid In</td>                   
                  <td>{{$offer_valid_type}}</b></td>
               </tr>
               @endif
               @if($offer_valid_type == 'GB Zone Wise')
                  <tr>
                    <td>Zones</td>
                    <td>
                      <ul>
                      @foreach($offer_zones as $zone)                                       
                        <li>@if($zone->zone)
                         {{$zone->zone->name}}
                         @endif</li>
                      @endforeach
                      </ul>
                    </td>
                  </tr>
               @elseif($offer_valid_type == 'Partner Wise') 
                  <tr>
                    <td>Partners</td>
                    <td>
                    <ul>
                    @foreach($offer_partners as $partner)                                    
                      <li>@if($partner->user)
                      {{$partner->user->name}}
                      @endif</li>
                    @endforeach
                    </ul>
                    </td>
                  </tr>
               @endif
                <tr>
                  <td>Description</td>                   
                  <td>{{$offer_info[0]->description}}</b></td>
               </tr>
              </table>
            </div>
         </div>
      </div>      
</section>
</div>
</div>
</div>
</div>
@endsection