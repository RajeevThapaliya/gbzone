@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Offers</h1>
		</div>
	<div class="search-data  col-md-12">
						{!! Form::open(['method'=>'GET','url' => 'admin/offers/','class'=>'navbar-form navbar-left','id'=>'search_form_id','novalidate' => 'novalidate']) !!}					
							<div class="form-group">
								{!! Form::label('Type', 'Type') !!}
								{!! Form::select('type',array(null=>'Please Select','birthday'=>'Birthday','general'=>'General'),$data['type'],array('class'=>'form-control','data-trigger'=>'change focusout')) !!}
							</div>
							<div class="form-group">
								{!! Form::label('Upcomming_expired', 'Upcomming/Expired Offers') !!}
								{!! Form::select('upcomming_expired',array(null=>'Please Select','upcomming'=>'Upcomming Offers','expired'=>'Expired Offers'),$data['upcomming_expired'],array('class'=>'form-control','data-trigger'=>'change focusout')) !!}
							</div>
							
							{!! Form::submit('Search',array('class'=>'btn btn-info btn-single','id'=>'search_id')) !!}

						{!! Form::close() !!}
					</div>
		<div class="wrapper-md">
			
			<div class="panel panel-default">

				<div class="panel-heading">
					<a href="{{url('admin/offers/create')}}" class="btn btn-sm btn-primary btn-addon"><i class="fa fa-plus"></i>Add New Offer</a>
				</div>

				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">SN</th>
								<th style="width:25%">Title</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if($offers)
							<?php $i = 1; ?>
							@foreach($offers as $offer)
								<tr>
									<td>{{$i}}</td>
									<td>{{$offer->title}}</td>
									<td>{{$offer->start_date}}</td>
									<td>{{$offer->end_date}}</td>
									<td>
										<a href="{{ route('admin.offers.show',$offer->id) }}" class="view btn btn-success btn-single" data-toggle="tooltip" title="Detail"><i class="fa fa-file-text-o"></i></a>

										<a class="edit btn btn-success btn-single" data-toggle="tooltip" title="Edit" href="{{ route('admin.offers.edit', $offer->id) }}"><i class="fa fa-edit"></i></a>
										{!! Form::open(['method' => 'DELETE', 'route'=>['admin.offers.destroy', $offer->id],'onsubmit' => 'return confirm("Do you want to delete this offer?")']) !!}
										{!!Form::button('<i class="icon icon-trash"></i> ', array(
									           'type' => 'submit',
									           'title' => 'Delete',
									           'data-toggle' => 'tooltip',
									           'class'=> 'btn btn-success btn-single',
									           'onclick'=>'return confirm("Are you sure?")'
									   	)); !!}
										{!! Form::close() !!}
										<a href="#" class="btn btn-sm btn-primary btn-addon">Re Run</a>
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@else
							No offers added yet.
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
