@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">New Offer</h1>
		</div>
		<div class="wrapper-md" ng-controller="FormDemoCtrl">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">Add New Offer</div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							{!! Form::open(['route' => 'admin.offers.store','data-parsley-validate','files'=>true]) !!}
								<?php if(Auth::user()->type == 'partner'):?>
									{!! Form::hidden('type','general',['id'=>'type']) !!}
								<?php endif;?>
								<div class="form-group">
									{!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
									{!! Form::text('title', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>
								<div class="form-group">
									{!! Form::label('image', 'Image:', ['class' => 'control-label']) !!}
									{!! Form::file('image', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>

								<?php if(Auth::user()->type == 'admin'):?>
								<div class="form-group">
									{!! Form::label('type', 'Type:', ['class' => 'control-label']) !!}
									{!! Form::select('type',array(null=>'Please Select')+$offer_type,null,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select offer', 'id'=>'offer_type')) !!}
								</div>
								<?php endif;?>
								<div class="form-group date_section" style="display: <?php (Auth::user()->type == 'admin')?'none':'block'?>;">
									{!! Form::label('start_date', 'Start Date:', ['class' => 'control-label']) !!}
									{!! Form::text('start_date', null, ['class' => 'form-control datepick required','data-trigger'=>'change focusout', 'id'=>'starting_date','ui-jq'=>'datepicker','ui-options'=>'{format: "yyyy-mm-dd",startDate: "$today_date"}','readonly'=>'readonly']) !!}
								</div>
								<div class="form-group date_section" style="display: <?php (Auth::user()->type == 'admin')?'none':'block'?>;">
									{!! Form::label('end_date', 'End Date:', ['class' => 'control-label']) !!}
									{!! Form::text('end_date', null, ['class' => 'form-control datepick required','data-trigger'=>'change focusout', 'id'=>'ending_date','ui-jq'=>'datepicker','ui-options'=>'{format: "yyyy-mm-dd",startDate: "$today_date"}','readonly'=>'readonly']) !!}
								</div>
								
								<?php if(Auth::user()->type == 'admin'):?>
									<div class="form-group">
										{!! Form::label('offer_valid_in', 'Offer Valid In:', ['class' => 'control-label']) !!}
										{!! Form::select('offer_valid_in',array(null=>'Please Select')+$offer_valid_in,null,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select','id'=>'offer_valid_in')) !!}
									</div>
									<div class="form-group" id="zone_section" style="display: none;">
										{!! Form::label('zone_id', 'Zone:', ['class' => 'control-label']) !!}
										{!! Form::select('zone_ids[]',array(null=>'Please Select')+$zones,null,array('class'=>'form-control','data-trigger'=>'change focusout','data-required-message'=>'Select','multiple'=>'multiple')) !!}
									</div>
									<div class="form-group" id="partner_section" style="display: none;">
										{!! Form::label('partner_id', 'Partner:', ['class' => 'control-label']) !!}
										{!! Form::select('partner_ids[]',array(null=>'Please Select')+$partners,null,array('class'=>'form-control','data-trigger'=>'change focusout','data-required-message'=>'Select','multiple'=>'multiple')) !!}
									</div>
								<?php endif;?>

								<div class="form-group">
									{!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
									{!! Form::textarea('description', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>
								<button type="submit" name="save_offer" class="btn btn-sm btn-primary">Submit</button>
<button type="submit" name="save_notify" value="save_and_notify" style="display: none;" class="btn btn-sm notify_save btn-primary">Submit And Push</button>
								<button type="reset" class="btn btn-sm btn-primary" onclick="location.href = '<?php echo URL::to('admin/offers');?>'">Cancel</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
