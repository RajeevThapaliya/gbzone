@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Change Password</h1>
		</div>
		<div class="wrapper-md" ng-controller="FormDemoCtrl">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">Change Password</div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							{!! Form::open(['route' => 'admin.updatepassword','id'=>'updatepassword','novalidate' => 'novalidate','role'=>'form']) !!}
								<div class="form-group">
									{!! Form::label('old_password', 'Old Password',['class'=>'control-label']) !!}
									{!! Form::password('old_password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter old password')) !!}
								</div>
								<div class="form-group">
									{!! Form::label('password', 'New Password',['class'=>'control-label']) !!}
									{!! Form::password('password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter password')) !!}
								</div>
								<div class="form-group">
									{!! Form::label('confirm_password', 'Confirm Password',['class'=>'control-label']) !!}
									{!! Form::password('confirm_password',array('class'=>'form-control required','data-trigger'=>'change focusout','data-equalto'=>'#password','data-required-message'=>'Please re-enter password','data-equalto-message'=>'Password didn\'t match with confirm password')) !!}
								</div>
								<button type="submit" name="change_password" class="btn btn-sm btn-primary">Change Password</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection