@extends('backend/app')
@section('content')
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Customer Detail</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
        <div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
          <!-- <div class="panel-heading font-bold">
            Create New Zone
          </div> -->
          <div class="panel-body">
            <div class="row">
            <?php ?>
            <table>
              
              <tr>
                  <td>Name</td>                   
                  <td>{{$user_info[0]->name}}</td>
               </tr>
               <tr>
                  <td>Email</td>                   
                  <td>{{$user_info[0]->email}}</td>
               </tr>
               <tr>
                  <td>Phone</td>                   
                  <td>{{$user_info[0]->phone}}</td>
               </tr>
               <tr>
                  <td>Qr Code</td>                   
                  <td>{{$user_info[0]->qr_code}}</td>
               </tr>
               <tr>
                  <td>address</td>                   
                  <td>{{$user_info[0]->zone}}</td>
               </tr>
              </table>
            </div>
         </div>
      </div>
</div>
</div>
</div>
</div>
@endsection
