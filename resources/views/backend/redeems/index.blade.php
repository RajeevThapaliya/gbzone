@extends('backend/app')
@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Redeemed Offers</h1>
		</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">Date</th>
								<th>Customer</th>
								<th style="width:25%">Offer Type</th>
								<?php if(Auth::user()->type == 'admin'):?>
									<th>Offer Claimed at</th>
								<?php endif;?>
								<th>Success Code</th>
							</tr>
						</thead>
						<tbody>
		
				@if($redeems)
		
							@foreach($redeems as $redeem)
				
								<tr>
									<td>{{$redeem->created_at}}</td>
					<td>
					{{--{{ route('admin.user.show',$redeem['offer_user']) }}--}}
					 <?php //echo $redeem['offer_user']; ?>
					@if($redeem['user'])
<a href="{{ route('admin.user.show',$redeem['offer_user']) }}" class="common-class view btn btn-success btn-single" data-toggle="tooltip" title="Detail"><i class="fa fa-file-text-o"></i></a>									
					{{$redeem['user']->name}}
					@endif
					</td>
							<td>									
								@if($redeem['offer'])
									{{ucfirst($redeem['offer']->type)}}
									@endif
								</td>
									<?php if(Auth::user()->type == 'admin'):?>
<td>										
@if($redeem['partner'])
										{{$redeem['partner']->name}}
										@endif
</td>
									<?php endif;?>
									
									<td>{{$redeem->redeem_code}}</td>
								
								</tr>
							@endforeach
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
