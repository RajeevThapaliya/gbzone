@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Update Partner</h1>
			</div>
			<div class="wrapper-md" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-12 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Update Partner
					</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::model($partner_info,['id'=>'partners-update','route' => ['admin.partners.update', $partner_info[0]->id], 'method' => 'patch','data-parsley-validate','files'=>true]) !!}
						{!! Form::hidden('id',$partner_info[0]->id,['id'=>'type']) !!}
						{!! Form::hidden('type','partner',['id'=>'type']) !!}
						{!! Form::hidden('latitude',($partner_info[0]->latitude)?$partner_info[0]->latitude:'27.7172453',['id'=>'latitude']) !!}
						{!! Form::hidden('longitude',($partner_info[0]->longitude)?$partner_info[0]->longitude:'85.3239605',['id'=>'longitude']) !!}
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
									{!! Form::text('name', $partner_info[0]->name, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('contact_person', 'Contact Person:', ['class' => 'control-label']) !!}
									{!! Form::text('contact_person', $partner_info[0]->contact_person, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
									{!! Form::text('email', $partner_info[0]->email, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('designation', 'Designation:', ['class' => 'control-label']) !!}
									{!! Form::text('designation', $partner_info[0]->designation, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
									{!! Form::text('city', $partner_info[0]->city, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
									
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
									{!! Form::text('address', $partner_info[0]->address, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('phone', 'Phone:', ['class' => 'control-label']) !!}
									{!! Form::input('number', 'phone', $value = $partner_info[0]->phone, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('zone_id', 'Zone:', ['class' => 'control-label']) !!}
									{!! Form::select('zone_id',array(null=>'Please Select')+$zones,$partner_info[0]->zone_id,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select zone')) !!}
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('logo', 'Logo:', ['class' => 'control-label']) !!}
									<div class="image_select">
										<input type="file" name="logo" id="file-1" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
										<label for="file-1"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a Image&hellip;</strong></label>
									</div>
									@if($partner_info[0]->logo != '')
										<img src="{{ asset('uploads/partners/logo/'.$partner_info[0]->logo)}}" style="height: 100px; margin-top: 10px;" />
									@endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('background_image', 'Background Image:', ['class' => 'control-label']) !!}
									<div class="image_select">
										<input type="file" name="background_image" id="file-2" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
										<label for="file-2"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a Image&hellip;</strong></label>
									</div>
									@if($partner_info[0]->background_image != '')
										<img src="{{ asset('uploads/partners/backgrounds/'.$partner_info[0]->background_image)}}" style="height: 100px; margin-top: 10px;" />
									@endif
								</div>
							</div>
						</div>

						<div class="form-group">
							
							<!-- @foreach($brands as $brand)
								{!! Form::checkbox('brand_id[]', $brand->id, in_array($brand->id, $partner_brands)?true:false) !!}
								{!! Form::label('brand_id', $brand->name) !!}
							@endforeach -->
							{!! Form::label('brand_id', 'Brands:') !!}
							<div class="checkbox">								
								@foreach($brands as $brand)
									<label class="i-checks i-checks-sm">
							            <input type="checkbox" name='brand_id[]' value="{{$brand->id}}" {{in_array($brand->id, $partner_brands)?'checked':''}}><i></i> {{$brand->name}}
							        </label>									
								@endforeach
							</div>
						</div>
						
						<div class="form-group">
							{!! Form::label('facebook_link', 'Facebook URL:', ['class' => 'control-label']) !!}
							{!! Form::text('facebook_link', $partner_info[0]->facebook_link, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							<input id='search_address' name="search_address" class='form-control' type='text' value='kathmandu, Nepal' placeholder='Search Box'>
							<div id="display_map" style="height:500px"></div>							
						</div>

						
						<button type="submit" name="save_zone" class="btn btn-sm btn-primary">Submit</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
