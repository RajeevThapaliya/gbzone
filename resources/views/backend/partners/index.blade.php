@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Partners</h1>
		</div>
<div class="container">
		
		<a href="{{ asset('uploads/partners/partners.xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a>
		
		<form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('admin/importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="file" name="import_file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
			<button type="submit" class="btn btn-primary">Import File</button>
		</form>
	</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{url('admin/partners/create')}}" class="btn btn-sm btn-primary btn-addon"><i class="fa fa-plus"></i>Add New Partner</a>
				</div>
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">SN</th>
								<th style="width:25%">Name</th>
								<th>Contact Person</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if($partners)
							<?php $i = 1;?>
							@foreach($partners as $partner)
								<tr>
									<td>{{$i}}</td>
									<td>{{$partner->name}}</td>
									<td>{{$partner->contact_person}}</td>
									<td>
										<a href="{{ route('admin.partners.show',$partner->id) }}" class="view btn btn-success btn-single" data-toggle="tooltip" title="Detail"><i class="fa fa-file-text-o"></i></a>
										<a class="edit btn btn-success btn-single" data-toggle="tooltip" title="Edit" href="{{ route('admin.partners.edit', $partner->id) }}"><i class="fa fa-edit"></i></a>
										{!! Form::open(['method' => 'DELETE','class'=>'common-class' ,'route'=>['admin.partners.destroy', $partner->id],'onsubmit' => 'return confirm("Do you want to delete this partner?")']) !!}
										<!-- {!!Form::button('<i class="icon icon-trash"></i> ', array('type' => 'submit')); !!} -->
										{!!Form::button('<i class="icon icon-trash"></i> ', array(
									           'type' => 'submit',
									           'title' => 'Delete',
									           'data-toggle' => 'tooltip',
									           'class'=> 'btn btn-success btn-single',
									           'onclick'=>'return confirm("Are you sure?")'
									   	)); !!}
										{!! Form::close() !!}
										<a data-toggle="tooltip" title="Reset Password" href="javascript:void(0)" class="edit btn btn-success btn-single reset_password" id="{{$partner->id}}" data-partnername="{{$partner->name}}">Reset</a>
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@else
							No Partners added yet.
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="partnerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Reset Password for <span class="partnername"></span></h4>
			</div>
			{!! Form::open(['route'=>['admin.partners.reset_password'],'id'=>'reset_password_form']) !!}
			{!! Form::hidden('id', '',array('id'=>'reset_partner_id')) !!}
			<div class="modal-body">
					<div class="form-group">
						{!! Form::label('new_password', 'New Password',['class'=>'control-label']) !!}
						{!! Form::password('new_password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please enter new password')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('password', 'Retype Password',['class'=>'control-label']) !!}
						{!! Form::password('confirm_password',array('class'=>'form-control required ','data-trigger'=>'change focusout','data-required-message'=>'Please retype password')) !!}
					</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default cancel_change" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary change_password">Rest Password</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="partnerResetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Reset Password for <span class="partnername"></span></h4>
			</div>
			<div class="modal-body">					

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default cancel_change" data-dismiss="modal">Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection
