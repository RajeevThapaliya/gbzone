@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Create New Partner</h1>
			</div>
			<div class="wrapper-md" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-12 col-xs-12">
				<div class="panel panel-default">
					<!-- <div class="panel-heading font-bold">
						Create New Partner
					</div> -->
					<div class="panel-body">
						<!-- @if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif -->
						{!! Form::open(['route' => 'admin.partners.store','id'=>'partners-create','data-parsley-validate','files'=>true]) !!}
						{!! Form::hidden('type','partner',['id'=>'type']) !!}
						{!! Form::hidden('latitude','27.7172453',['id'=>'latitude']) !!}
						{!! Form::hidden('longitude','85.3239605',['id'=>'longitude']) !!}
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
									{!! Form::text('name', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('contact_person', 'Contact Person:', ['class' => 'control-label']) !!}
									{!! Form::text('contact_person', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('contact_person')) <p class="help-block">{{ $errors->first('contact_person') }}</p> @endif
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
									{!! Form::text('email', null, ['class' => 'form-control email required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('designation', 'Designation:', ['class' => 'control-label']) !!}
									{!! Form::text('designation', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('designation')) <p class="help-block">{{ $errors->first('designation') }}</p> @endif
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
									{!! Form::text('city', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
								</div>
								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
									{!! Form::text('address', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
								</div>
								
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('phone', 'Phone:', ['class' => 'control-label']) !!}
									{!! Form::input('number', 'phone', $value = null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

									@if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('zone_id', 'Zone:', ['class' => 'control-label']) !!}
									{!! Form::select('zone_id',array(null=>'Please Select Zone')+$zones,null,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select zone')) !!}

									@if ($errors->has('zone_id')) <p class="help-block">{{ $errors->first('zone_id') }}</p> @endif
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('logo', 'Logo:', ['class' => 'control-label']) !!}
									<div class="image_select">
										<input type="file" name="logo" id="file-1" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
										<label for="file-1"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a Image&hellip;</strong></label>
									</div>										
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('background_image', 'Background Image:', ['class' => 'control-label']) !!}
									<div class="image_select">
										<input type="file" name="background_image" id="file-2" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
										<label for="file-2"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a Image&hellip;</strong></label>
									</div>										
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="checkbox-lebel">
								{!! Form::label('brand_id', 'Brands:') !!}
							</div>							
							<div class="checkbox">								
								@foreach($brands as $brand)
									<label class="i-checks i-checks-sm">
							            <input type="checkbox" name='brand_id[]' value="{{$brand->id}}"><i></i> {{$brand->name}}
							        </label>									
								@endforeach
								@if ($errors->has('brand_id')) <p class="help-block">{{ $errors->first('brand_id') }}</p> @endif
							</div>
						</div>

						<div class="form-group">
							{!! Form::label('facebook_link', 'Facebook URL:', ['class' => 'control-label']) !!}
							{!! Form::text('facebook_link', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}
						</div>

						<div class="form-group">
							<input id='search_address' name="search_address" class='form-control' type='text' value='kathmandu, Nepal' placeholder='Search Box'>
							<div id="display_map" style="height:500px"></div>							
						</div>
						
						<button type="submit" name="save_zone" class="btn btn-sm btn-primary">Submit</button>
						<button type="reset" class="btn btn-sm btn-primary" onclick="location.href = '<?php echo URL::to('admin/partners');?>'">Cancel</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
