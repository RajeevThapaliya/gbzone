@extends('backend/app')
@section('content')
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Partner Detail</h1>
      </div>
      <div class="wrapper-md" ng-controller="FormDemoCtrl">
        <div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
          <!-- <div class="panel-heading font-bold">
            Create New Zone
          </div> -->
          {!! Form::hidden('latitude',$partner_info[0]->latitude,['id'=>'latitude']) !!}
            {!! Form::hidden('longitude',$partner_info[0]->longitude,['id'=>'longitude']) !!}
          <div class="panel-body">
            <div class="row">
            <?php ?>
            <table>
              <tr>
                  <td>Image</td>                   
                  <td><img src="{{ asset('uploads/partners/logo/'.$partner_info[0]->logo)}}" style="height: 100px; margin-top: 10px;" /></td>
               </tr>
              <tr>
                  <td>Name</td>                   
                  <td>{{$partner_info[0]->name}}</td>
               </tr>
               <tr>
                  <td>Email</td>                   
                  <td>{{$partner_info[0]->email}}</td>
               </tr>
               <tr>
                  <td>Phone</td>                   
                  <td>{{$partner_info[0]->phone}}</td>
               </tr>
               <tr>
                  <td>Qr Code</td>                   
                  <td>{{$partner_info[0]->qr_code}}</td>
               </tr>
               <tr>
                  <td>Zone</td>                   
                  <td>{{$partner_info[0]->zone}}</td>
               </tr>
               <tr>
                  <td>Background Image</td>                   
                  <td><img src="{{ asset('uploads/partners/backgrounds/'.$partner_info[0]->background_image)}}" style="height: 100px; margin-top: 10px;" /></td>
               </tr>
                <tr>
                  <td>Description</td>                   
                  <td>{{$partner_info[0]->description}}</b></td>
               </tr>
              </table>
            </div>
         </div>
      </div>
      <div class="form-group">
           <div id="display_map" style="height:500px"></div>             
      </div>      
</div>
</div>
</div>
</div>
@endsection
