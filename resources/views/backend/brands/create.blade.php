@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">New Brand</h1>
		</div>
		<div class="wrapper-md" ng-controller="FormDemoCtrl">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<!-- <div class="panel-heading font-bold">Add New Brand</div> -->
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<!-- @if (count($errors) > 0)
										<div class="alert alert-danger">
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif -->
									
									{!! Form::open(['route' => 'admin.brands.store','id'=>'brands-create','data-parsley-validate','files'=>true]) !!}
										<div class="form-group">
											{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
											{!! Form::text('name', null, ['class' => 'form-control required','data-trigger'=>'change focusout']) !!}

											@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
										</div>	
										<div class="form-group">
											{!! Form::label('image', 'Image:', ['class' => 'control-label']) !!}
											<!-- {!! Form::file('image', null, ['class' => 'form-control']) !!} -->
											<div class="image_select">
												<input type="file" name="image" id="file-1" class="inputfile inputfile-6 required" data-trigger="change focusout" data-multiple-caption="{count} files selected" multiple />
												<label for="file-1"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a Image&hellip;</strong></label>
											</div>										
										</div>	
										<button type="submit" name="save_brand" class="btn btn-sm btn-primary">Submit</button>
										<button type="reset" class="btn btn-sm btn-primary" onclick="location.href = '<?php echo URL::to('admin/brands');?>'">Cancel</button>
									</form>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection