@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Brands</h1>
		</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{url('admin/brands/create')}}" class="btn btn-sm btn-primary btn-addon"><i class="fa fa-plus"></i>Add New Brand</a>
				</div>
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">SN</th>
								<th style="width:25%">name</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if ($brands)
							<?php $i = 1; ?>
							@foreach($brands as $brand)
								<tr>
									<td>{{$i}}</td>
									<td>{{$brand->name}}</td>
									<td>
										<a class="edit btn btn-success btn-single" data-toggle="tooltip" title="Edit" href="{{ route('admin.brands.edit', $brand->id) }}"><i class="fa fa-edit"></i></a>
										{!! Form::open(['method' => 'DELETE', 'route'=>['admin.brands.destroy', $brand->id],'onsubmit' => 'return confirm("Do you want to delete this brand?")']) !!}
										<!-- {!!Form::button('<i class="icon icon-trash"></i> ', array('type' => 'submit')); !!} -->
										{!!Form::button('<i class="icon icon-trash"></i> ', array(
									           'type' => 'submit',
									           'title' => 'Delete',
									           'data-toggle' => 'tooltip',
									           'class'=> 'btn btn-success btn-single',
									           'onclick'=>'return confirm("Are you sure?")'
									   	)); !!}
										{!! Form::close() !!}
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@else
							No Brands added yet
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection