@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="hbox hbox-auto-xs hbox-auto-sm">
			<!-- main -->
			<div class="col">
				<!-- main header -->
				<div class="bg-light lter b-b wrapper-md">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="m-n font-thin h3 text-black">Dashboard</h1>
								<small class="text-muted">Welcome to GBApp</small>
						</div>
					</div>
				</div>
				<!-- / main header -->
				<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
					<!-- stats -->
					<div class="row">
						<div class="col-md-12">
							<div class="row row-sm">
								<?php if(Auth::user()->type == 'admin'):?>
									<div class="col-md-2">
										<div class="panel padder-v item">
											<div class="widget-icon bg-blue">
												<i class="icon-briefcase"></i>	
											</div>
											<div class="widget-details">
												<span class="count_numbers">
													{{$brand_count}}
												</span>
											</div>
										</div>
										<a href="{{url('admin/brands')}}">
											<div class="widget_link">
												<span class="title">Brands</span>
											</div>
										</a>
									</div>
									<div class="col-md-2">
										<div href="" class="block panel padder-v item">
											<div class="widget-icon bg-danger">
												<i class="icon-globe icon"></i>
											</div>
											<div class="widget-details">
												<span class="count_numbers">
													{{$zones_count}}
												</span>
											</div>
										</div>
										<a href="{{url('admin/zones')}}">
											<div class="widget_link">
												<span class="title">Zones</span>
											</div>
										</a>
									</div>
									<div class="col-md-2">
										<div href="" class="block panel padder-v item">
											<div class="widget-icon bg-success">
												<i class="icon-users"></i>
											</div>
											<div class="widget-details">
												<span class="count_numbers">
													{{$partner_count}}
												</span>
											</div>
										</div>
										<a href="{{url('admin/partners')}}">
											<div class="widget_link">
												<span class="title">Partners</span>
											</div>
										</a>
									</div>
								<?php endif;?>
								<?php if(in_array(Auth::user()->type, array('admin','partner'))):?>
									<div class="col-md-2">
										<div class="panel padder-v item">
											<div class="widget-icon bg-warning">
												<i class="icon-calendar"></i>
											</div>
											<div class="widget-details">
												<span class="count_numbers">
													{{$event_count}}
												</span>
											</div>
										</div>
										<a href="{{url('admin/events')}}">
											<div class="widget_link">
												<span class="title">Events</span>
											</div>
										</a>
									</div>
									<div class="col-md-2">
										<div class="panel padder-v item">
											<div class="widget-icon bg-gray">
												<i class="icon-handbag"></i>
											</div>
											<div class="widget-details">
												<span class="count_numbers">
													{{$offer_count}}
												</span>	
											</div>											
										</div>
										<a href="{{url('admin/offers')}}">
											<div class="widget_link">
												<span class="title">Offers</span>
											</div>
										</a>
									</div>
								<?php endif;?>
							</div>
						</div>
					</div>
				<!-- / stats -->
				</div>
			</div>
			<!-- / main -->
		</div>
	</div>
</div>
@endsection