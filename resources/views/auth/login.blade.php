@extends('backend/app')

@section('content')
    <!-- Meassage-->
    @if(Session::has('error-message') || Session::has('success-message'))
                
        @if ($alert  = Session::get('error-message'))                        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
            </button>
            <strong>{{ $alert }}</strong>
        </div>
                                        
        @elseif($alert = Session::get('success-message'))                        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
            </button>
            
            <strong>{{ $alert }}</strong>
        </div>
            
        @endif
    @endif
<div class="app app-header-fixed  ">

	<div class="container w-xxl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
		<a href class="navbar-brand block m-t">GB App</a>
		<div class="m-b-lg">
			<div class="wrapper text-center">
				<strong>Sign in to get in touch</strong>
			</div>


			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<form class="form-horizontal" id="login-form" data-parsley-validate="data-parsley-validate" role="form" method="POST" action="{{ url('/auth/login') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="text-danger wrapper text-center" ng-show="authError"></div>
				<div class="list-group list-group-sm">
					<div class="list-group-item">
						<input type="email" name="email" class="form-control required no-border" data-trigger='change focusout' data-required-message='Please enter email' placeholder="Email"  value="{{ old('email') }}" >
					</div>
					<div class="list-group-item">
						<input type="password" name="password" placeholder="Password" class="form-control required no-border" data-trigger='change focusout' data-required-message='Please enter password'>
					</div>
				</div>
				<button type="submit" class="btn btn-lg btn-primary btn-block">Log in</button>
			</form>
	<div class="container">
  <a href="#" data-target="#pwdModal" data-toggle="modal">Forgot my password</a>
</div>
		</div>
	</div>
</div>


<!--modal-->
<div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center">What's My Password?</h1>
      </div>
      <div class="modal-body">
          <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                          
                          <p>If you have forgotten your password you can reset it here.</p>
                            <div class="panel-body">
                            	<form class="form-horizontal" role="form" method="POST" action="{{ route('admin.newpassword') }}">
	                                <fieldset>
	                                	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	                                    <div class="form-group">
	                                        <input class="form-control input-lg" placeholder="E-mail Address" name="email" type="email">
	                                    </div>
	                                    <input class="btn btn-lg btn-primary btn-block" value="Send My Password" type="submit">
	                                </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		  </div>	
      </div>
  </div>
  </div>
</div>

@endsection
