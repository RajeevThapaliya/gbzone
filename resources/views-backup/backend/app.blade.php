<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>GB App</title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

		<link rel="stylesheet" href="{{ asset('/backend/bower_components/bootstrap/dist/css/bootstrap.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('/backend/bower_components/animate.css/animate.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('/backend/bower_components/font-awesome/css/font-awesome.min.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('/backend/bower_components/simple-line-icons/css/simple-line-icons.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('/backend/css/font.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('/backend/css/app.css')}}" type="text/css" />
	</head>
	<body>
		@if(!Request::is('auth/login'))
			<div class="app app-header-fixed  ">
				@include('backend/navigation')
				@yield('content')
				@include('backend/footer')
			</div>		
		@else
			@yield('content')
		@endif
		<script type="text/javascript">
			var app_url = '<?php echo URL::to('/')?>';
		</script>
		<script src="{{ asset('/backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
		<script src="{{ asset('/backend/bower_components/bootstrap/dist/js/bootstrap.js')}}"></script>
		<script src="{{ asset('/backend/js/ui-load.js')}}"></script>
		<script src="{{ asset('/backend/js/ui-jp.config.js')}}"></script>
		<script src="{{ asset('/backend/js/ui-jp.js')}}"></script>
		<script src="{{ asset('/backend/js/ui-nav.js')}}"></script>
		<script src="{{ asset('/backend/js/ui-toggle.js')}}"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCODg6PYVfZQV9XOWUE0kApv7_FHF8nolM&libraries=places"></script>
		<script src="{{ asset('/backend/js/custom_scripts.js')}}"></script>
	</body>
</html>