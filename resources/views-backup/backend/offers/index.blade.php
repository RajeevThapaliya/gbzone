@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Offers</h1>
		</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{url('admin/offers/create')}}" class="btn btn-primary">Add New Offer</a>
				</div>
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">SN</th>
								<th style="width:25%">Title</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if($offers)
							<?php $i = 1; ?>
							@foreach($offers as $offer)
								<tr>
									<td>{{$i}}</td>
									<td>{{$offer->title}}</td>
									<td>{{$offer->start_date}}</td>
									<td>{{$offer->end_date}}</td>
									<td>
										Re Run
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@else
							No offers added yet.
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection