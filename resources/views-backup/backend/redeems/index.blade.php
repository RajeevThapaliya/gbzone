@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Redeemed Offers</h1>
		</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">Date</th>
								<th style="width:25%">Offer Type</th>
								<th>Offer Claimed at</th>
								<th>Success Code</th>
							</tr>
						</thead>
						<tbody>
						@if(!$redeems)
							@foreach($redeems as $redeem)
								<tr>
									<td>{{$redeem->created_at}}</td>
									<td>{{ucfirst($redeem['offer']->type)}}</td>
									<td>{{$redeem['partner']->name}}</td>
									<td>{{$redeem['offer']->offer_code}}</td>
								</tr>
							@endforeach
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection