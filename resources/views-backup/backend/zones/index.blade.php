@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Datatable</h1>
		</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{url('admin/zones/create')}}" class="btn btn-primary">Add New Zone</a>
				</div>
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">SN</th>
								<th style="width:25%">Name</th>
								<th>District</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if($zones)
							<?php $i = 1; ?>
							@foreach($zones as $zone)
								<tr>
									<td>{{$i}}</td>
									<td>{{$zone->name}}</td>
									<td>{{$zone->district}}</td>
									<td>
										<a href="{{ route('admin.zones.edit', $zone->id) }}"><i class="fa fa-edit"></i></a>
										{!! Form::open(['method' => 'DELETE', 'route'=>['admin.zones.destroy', $zone->id],'onsubmit' => 'return confirm("Do you want to delete this zone?")']) !!}
										{!!Form::button('<i class="icon icon-trash"></i> ', array('type' => 'submit')); !!}
										{!! Form::close() !!}
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@else
							No zones added yet.
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection