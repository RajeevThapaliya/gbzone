@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Create New Zone</h1>
			</div>
			<div class="wrapper-md container" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Create New Zone
					</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::open(['route' => 'admin.zones.store','id'=>'zones-create','data-parsley-validate']) !!}
						{!! Form::hidden('latitude','27.7172453',['id'=>'latitude']) !!}
						{!! Form::hidden('longitude','85.3239605',['id'=>'longitude']) !!}
						<div class="form-group">
							{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
							{!! Form::text('name', null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('district', 'District:', ['class' => 'control-label']) !!}
							{!! Form::text('district', null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<input id='search_address' name="search_address" class='form-control' type='text' value='kathmandu, Nepal' placeholder='Search Box'>
							<div id="display_map" style="height:500px"></div>							
						</div>
								<div class="form-group">
							{!! Form::label('radius', 'Radius:', ['class' => 'control-label']) !!}
							{!! Form::text('radius', null, ['class' => 'form-control']) !!}
						</div>
						<button type="submit" name="save_zone" class="btn btn-sm btn-primary">Submit</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
