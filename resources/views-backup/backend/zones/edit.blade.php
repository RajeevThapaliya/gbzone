@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Update Zone</h1>
			</div>
			<div class="wrapper-md container" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Update Zone
					</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::model($zone_info,['id'=>'brand-update','route' => ['admin.zones.update', $zone_info[0]->id], 'method' => 'patch','data-parsley-validate']) !!}
						{!! Form::hidden('latitude',$zone_info[0]->latitude,['id'=>'latitude']) !!}
						{!! Form::hidden('longitude',$zone_info[0]->longitude,['id'=>'longitude']) !!}
						<div class="form-group">
							{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
							{!! Form::text('name', $zone_info[0]->name, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('district', 'District:', ['class' => 'control-label']) !!}
							{!! Form::text('district', $zone_info[0]->district, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<input id='search_address' name="search_address" class='form-control' type='text' value='kathmandu, Nepal' placeholder='Search Box'>
							<div id="display_map" style="height:500px"></div>							
						</div>
								<div class="form-group">
							{!! Form::label('radius', 'Radius:', ['class' => 'control-label']) !!}
							{!! Form::text('radius', $zone_info[0]->radius, ['class' => 'form-control']) !!}
						</div>
						<button type="submit" name="save_zone" class="btn btn-sm btn-primary">Submit</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
