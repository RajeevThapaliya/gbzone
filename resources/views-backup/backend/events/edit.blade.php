@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Update Event</h1>
			</div>
			<div class="wrapper-md container" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Update Event
					</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::model($event_info,['id'=>'brand-update','route' => ['admin.events.update', $event_info[0]->id], 'method' => 'patch','data-parsley-validate','files'=>true]) !!}
						{!! Form::hidden('latitude',$event_info[0]->latitude,['id'=>'latitude']) !!}
						{!! Form::hidden('longitude',$event_info[0]->longitude,['id'=>'longitude']) !!}
						<div class="form-group">
							{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
							{!! Form::text('name', $event_info[0]->name, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('image', 'Image:', ['class' => 'control-label']) !!}
							{!! Form::file('image', null, ['class' => 'form-control']) !!}
							@if($event_info[0]->image != '')
								<img src="{{ asset('uploads/'.$event_info[0]->image)}}" style="height: 100px; margin-top: 10px;" />
							@endif
						</div>

						<div class="form-group">
							{!! Form::label('type', 'Type:', ['class' => 'control-label']) !!}
							{!! Form::select('type',array(null=>'Please Select')+$event_type,$event_info[0]->type,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select type')) !!}
						</div>

						<div class="form-group">
							{!! Form::label('start_date', 'Start Date:', ['class' => 'control-label']) !!}
							{!! Form::text('start_date', $event_info[0]->start_date, ['class' => 'form-control datepick','ui-jq'=>'datepicker','ui-options'=>'{format: "yyyy-mm-dd",startDate: "$today_date"}','readonly'=>'readonly']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('end_date', 'End Date:', ['class' => 'control-label']) !!}
							{!! Form::text('end_date', $event_info[0]->end_date, ['class' => 'form-control datepick','ui-jq'=>'datepicker','ui-options'=>'{format: "yyyy-mm-dd",startDate: "$today_date"}','readonly'=>'readonly']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
							{!! Form::text('city', $event_info[0]->city, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('location', 'Location:', ['class' => 'control-label']) !!}
							{!! Form::text('location', $event_info[0]->location, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('invite_to', 'Invite To:', ['class' => 'control-label']) !!}
							@foreach($invite_to as $k => $v)
								{!! Form::radio('invite_to',$k, ($k == $event_info[0]->invite_to)?true:false) !!}
								{!! Form::label($v) !!}
							@endforeach
						</div>

						<div class="form-group">
							{!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
							{!! Form::textarea('description', $event_info[0]->description, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							<input id='search_address' name="search_address" class='form-control' type='text' value='kathmandu, Nepal' placeholder='Search Box'>
							<div id="display_map" style="height:500px"></div>							
						</div>
						
						<button type="submit" name="save_event" class="btn btn-sm btn-primary">Submit</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
