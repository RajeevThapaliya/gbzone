@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Update Partner</h1>
			</div>
			<div class="wrapper-md container" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Update Partner
					</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::model($partner_info,['id'=>'partners-update','route' => ['admin.partners.update', $partner_info[0]->id], 'method' => 'patch','data-parsley-validate']) !!}
						{!! Form::hidden('type','partner',['id'=>'type']) !!}
						<div class="form-group">
							{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
							{!! Form::text('name', $partner_info[0]->name, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('contact_person', 'Contact Person:', ['class' => 'control-label']) !!}
							{!! Form::text('contact_person', $partner_info[0]->contact_person, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
							{!! Form::text('email', $partner_info[0]->email, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('designation', 'Designation:', ['class' => 'control-label']) !!}
							{!! Form::text('designation', $partner_info[0]->designation, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
							{!! Form::text('address', $partner_info[0]->address, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
							{!! Form::text('city', $partner_info[0]->city, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('phone', 'Phone:', ['class' => 'control-label']) !!}
							{!! Form::text('phone', $partner_info[0]->phone, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('zone_id', 'Zone:', ['class' => 'control-label']) !!}
							{!! Form::select('zone_id',array(null=>'Please Select')+$zones,$partner_info[0]->zone_id,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select zone')) !!}
						</div>

						<div class="form-group">
							{!! Form::label('brand_id', 'Brands:', ['class' => 'control-label']) !!}
							@foreach($brands as $brand)
								{!! Form::checkbox('brand_id[]', $brand->id, in_array($brand->id, $partner_brands)?true:false) !!}
								{!! Form::label('brand_id', $brand->name) !!}
							@endforeach
						</div>

						
						<button type="submit" name="save_zone" class="btn btn-sm btn-primary">Submit</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
