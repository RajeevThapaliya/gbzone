@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">Partners</h1>
		</div>
		<div class="wrapper-md">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a href="{{url('admin/partners/create')}}" class="btn btn-primary">Add New Partner</a>
				</div>
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="" class="table table-striped m-b-none">
						<thead>
							<tr>
								<th style="width:20%">SN</th>
								<th style="width:25%">Name</th>
								<th>Contact Person</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						@if($partners)
							<?php $i = 1;?>
							@foreach($partners as $partner)
								<tr>
									<td>{{$i}}</td>
									<td>{{$partner->name}}</td>
									<td>{{$partner->contact_person}}</td>
									<td>
										<a href="{{ route('admin.partners.edit', $partner->id) }}"><i class="fa fa-edit"></i></a>
										{!! Form::open(['method' => 'DELETE', 'route'=>['admin.partners.destroy', $partner->id],'onsubmit' => 'return confirm("Do you want to delete this partner?")']) !!}
										{!!Form::button('<i class="icon icon-trash"></i> ', array('type' => 'submit')); !!}
										{!! Form::close() !!}
									</td>
								</tr>
								<?php $i++; ?>
							@endforeach
						@else
							No Partners added yet.
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection