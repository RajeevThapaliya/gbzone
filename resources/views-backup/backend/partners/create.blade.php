@extends('backend/app')

@section('content')
	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Create New Partner</h1>
			</div>
			<div class="wrapper-md container" ng-controller="FormDemoCtrl">
				<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
				<div class="panel panel-default">
					<div class="panel-heading font-bold">
						Create New Partner
					</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						{!! Form::open(['route' => 'admin.partners.store','id'=>'partners-create','data-parsley-validate']) !!}
						{!! Form::hidden('type','partner',['id'=>'type']) !!}
						<div class="form-group">
							{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
							{!! Form::text('name', null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('contact_person', 'Contact Person:', ['class' => 'control-label']) !!}
							{!! Form::text('contact_person', null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
							{!! Form::text('email', null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('designation', 'Designation:', ['class' => 'control-label']) !!}
							{!! Form::text('designation', null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
							{!! Form::text('address', null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
							{!! Form::text('city', null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('phone', 'Phone:', ['class' => 'control-label']) !!}
							{!! Form::text('phone', null, ['class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('zone_id', 'Zone:', ['class' => 'control-label']) !!}
							{!! Form::select('zone_id',array(null=>'Please Select')+$zones,null,array('class'=>'form-control required','data-trigger'=>'change focusout','data-required-message'=>'Select zone')) !!}
						</div>

						<div class="form-group">
							{!! Form::label('brand_id', 'Brands:', ['class' => 'control-label']) !!}
							@foreach($brands as $brand)
								{!! Form::checkbox('brand_id[]', $brand->id) !!}
								{!! Form::label('brand_id', $brand->name) !!}
							@endforeach
						</div>

						
						<button type="submit" name="save_zone" class="btn btn-sm btn-primary">Submit</button>
						</form>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection
