@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="bg-light lter b-b wrapper-md">
			<h1 class="m-n font-thin h3">New Brand</h1>
		</div>
		<div class="wrapper-md" ng-controller="FormDemoCtrl">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading font-bold">Add New Brand</div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							{!! Form::open(['route' => 'admin.brands.store','id'=>'brands-create','data-parsley-validate']) !!}
								<div class="form-group">
									{!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
									{!! Form::text('name', null, ['class' => 'form-control']) !!}
								</div>		
								<button type="submit" name="save_brand" class="btn btn-sm btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection