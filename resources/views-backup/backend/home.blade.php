@extends('backend/app')

@section('content')
<div id="content" class="app-content" role="main">
	<div class="app-content-body ">
		<div class="hbox hbox-auto-xs hbox-auto-sm">
			<!-- main -->
			<div class="col">
				<!-- main header -->
				<div class="bg-light lter b-b wrapper-md">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="m-n font-thin h3 text-black">Dashboard</h1>
								<small class="text-muted">Welcome to GBApp</small>
						</div>
					</div>
				</div>
				<!-- / main header -->
				<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
					<!-- stats -->
					<div class="row">
						<div class="col-md-5">
							<div class="row row-sm text-center">
								<?php if(Auth::user()->type == 'admin'):?>
									<div class="col-xs-6">
										<div class="panel padder-v item">
											<div class="h1 text-info font-thin h1">{{$brand_count}}</div>
												<span class="text-muted text-xs">Brands</span>
												<div class="top text-right w-full">
													<i class="fa fa-caret-down text-warning m-r-sm"></i>
												</div>
										</div>
									</div>
									<div class="col-xs-6">
										<a href="" class="block panel padder-v bg-primary item">
											<span class="text-white font-thin h1 block">{{$zones_count}}</span>
											<span class="text-muted text-xs">Zones</span>
											<span class="bottom text-right w-full">
												<i class="fa fa-cloud-upload text-muted m-r-sm"></i>
											</span>
										</a>
									</div>
									<div class="col-xs-6">
										<a href="" class="block panel padder-v bg-info item">
											<span class="text-white font-thin h1 block">{{$partner_count}}</span>
											<span class="text-muted text-xs">Partners</span>
											<span class="top text-left">
												<i class="fa fa-caret-up text-warning m-l-sm"></i>
											</span>
										</a>
									</div>
								<?php endif;?>
								<?php if(in_array(Auth::user()->type, array('admin','partner'))):?>
									<div class="col-xs-6">
										<div class="panel padder-v item">
											<div class="font-thin h1">{{$event_count}}</div>
											<span class="text-muted text-xs">Events</span>
											<div class="bottom text-left">
												<i class="fa fa-caret-up text-warning m-l-sm"></i>
											</div>
										</div>
									</div>
									<div class="col-xs-6">
										<div class="panel padder-v item">
											<div class="font-thin h1">{{$offer_count}}</div>
											<span class="text-muted text-xs">Offers</span>
											<div class="bottom text-left">
												<i class="fa fa-caret-up text-warning m-l-sm"></i>
											</div>
										</div>
									</div>
								<?php endif;?>
							</div>
						</div>
					</div>
				<!-- / stats -->
				</div>
			</div>
			<!-- / main -->
		</div>
	</div>
</div>
@endsection