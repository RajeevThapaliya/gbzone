@extends('backend/app')

@section('content')
<div class="app app-header-fixed  ">
	<div class="container w-xxl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
		<a href class="navbar-brand block m-t">GB App</a>
		<div class="m-b-lg">
			<div class="wrapper text-center">
				<strong>Sign in to get in touch</strong>
			</div>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="text-danger wrapper text-center" ng-show="authError"></div>
				<div class="list-group list-group-sm">
					<div class="list-group-item">
						<input type="email" name="email" placeholder="Email" class="form-control no-border" value="{{ old('email') }}" required>
					</div>
					<div class="list-group-item">
						<input type="password" name="password" placeholder="Password" class="form-control no-border" required>
					</div>
				</div>
				<button type="submit" class="btn btn-lg btn-primary btn-block">Log in</button>
			</form>
		</div>
	</div>
</div>
@endsection
