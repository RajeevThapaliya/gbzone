    $(document).ready(function () {
        if($("#display_map").length == 1){
            initialize();
            function initialize() {
                // Define the latitude and longitude positions
                var latitude = parseFloat(document.getElementById('latitude').value); // Latitude get from above variable
                var longitude = parseFloat(document.getElementById('longitude').value); // Longitude from same
                var latlngPos = new google.maps.LatLng(latitude, longitude);

                

                // Set up options for the Google map
                var myOptions = {
                    zoom: 15,
                    center: latlngPos,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControlOptions: true,
                    zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE
                    }
                };
                // Define the map
                map = new google.maps.Map(document.getElementById("display_map"), myOptions);

                // Add the marker
                var marker = new google.maps.Marker({
                    position: latlngPos,
                    map: map,
                    draggable: true,
                });

                addMarker(latlngPos, 'Default Marker', map);
      
                google.maps.event.addListener(map, 'dragstart', function(event) {
                    //infowindow.open(map,marker);
                    //addMarker(event.latlngPos, 'Click Generated Marker', map);     
                    //var lat, lng, address;
                });

                google.maps.event.addListener(marker,'dragend',function(event) {
                    document.getElementById('latitude').value = event.latLng.lat();
                    document.getElementById('longitude').value = event.latLng.lng();
                    //alert(marker.getPosition());
                });
                
                var input = document.getElementById('search_address');
                var searchBox = new google.maps.places.SearchBox(input);
                //console.log(searchBox);
                //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.addListener('bounds_changed', function() {
                  searchBox.setBounds(map.getBounds());
                });
                var markers = [];
                searchBox.addListener('places_changed', function() {
                    
                    var places = searchBox.getPlaces();
                    if (places.length == 0) {
                        return;
                    }
                    markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function(place) {
                        if (!place.geometry) {
                          console.log("Returned place contains no geometry");
                          return;
                        }
                        //console.log(place.geometry.location.lng());
                        var icon = {
                          size: new google.maps.Size(71, 71),
                          origin: new google.maps.Point(0, 0),
                          anchor: new google.maps.Point(17, 34),
                          scaledSize: new google.maps.Size(25, 25)
                        };
                        markers.push(new google.maps.Marker({
                            map: map,
                            // icon: icon,
                            // title: place.name,
                            position: place.geometry.location,
                            draggable: true,
                            zoom: 13
                        }));
                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }
                        document.getElementById('latitude').value = place.geometry.location.lat();
                        document.getElementById('longitude').value = place.geometry.location.lng();
                    });
                    map.fitBounds(bounds);
                });
                
            }

            function addMarker(latlng,title,map) {
                var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: title,
                        icon:'map-red.png',
                        draggable:true,
                        animation: google.maps.Animation.DROP
                });

                google.maps.event.addListener(marker,'drag',function(event) {
                    document.getElementById('latitude').value = event.latLng.lat();
                    document.getElementById('longitude').value= event.latLng.lng();
                });

                google.maps.event.addListener(marker,'dragend',function(event) {
                    //alert("completed");
                    document.getElementById('latitude').value = event.latLng.lat();
                    document.getElementById('longitude').value = event.latLng.lng();
                    // alert(marker.getPosition());
                });
                google.maps.event.addListener(map, 'zoom_changed', function () {
                    // document.getElementById('zoom').value =map.getZoom();
                });
        
            }
            //        
            google.maps.event.addDomListener(window, 'load', initialize);

        }
        //
        if($("#offer_type").length == 1){
            $("#offer_type").on('change', function(){
                $type = $(this).val();
                if($type == 'general'){
                    $(".date_section").show();
                }else if($type == 'birthday'){
                    $(".date_section").hide();
                }else{
                    $(".date_section").hide();
                }
            });
        }

        if($("#offer_valid_in").length == 1){
            $("#offer_valid_in").on('change', function(){
                $offer_valid_in = $(this).val();
                if($offer_valid_in == 'zones'){
                    $("#zone_section").show();
                    $("#partner_section").hide();
                }else if($offer_valid_in == 'partners'){
                    $("#zone_section").hide();
                    $("#partner_section").show();
                }else{
                    $("#zone_section").hide();
                    $("#partner_section").hide();
                }
            });
        }
        
        $(document).on('keyup keypress', 'form input[name="search_address"]', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
        $('.datepick').on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
    });

///
