    $(document).ready(function () {

        window.ParsleyConfig = {
            errorsWrapper: '<div></div>',
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        };
        $("#brands-create").parsley();
        $("#brand-update").parsley();
        $("#events-create").parsley();
        $("#events-update").parsley();
        $("#offers-create").parsley();
        $("#offers-update").parsley();
        $("#partners-create").parsley();
        $("#partners-update").parsley();
        $("#updatepassword").parsley();
        $("#zones-create").parsley();
        $("#zones-update").parsley();
        $("#search_form_id").parsley();

        if($("#start_date").length == 1){
            $('#start_date').datetimepicker({
                format:'YYYY-MM-DD HH:mm',
                minDate: new Date(),
                ignoreReadonly: true
            }).on("dp.change", function(e) {
                // alert("selected");
                console.log(e.date);
            });
            $('#end_date').datetimepicker({
                format:'YYYY-MM-DD HH:mm',
                ignoreReadonly: true,
                useCurrent: false //Important! See issue #1075
            });
            $("#start_date").on("dp.change", function (e) {
                $('#end_date').data("DateTimePicker").minDate(e.date);
            });
            $("#end_date").on("dp.change", function (e) {
                $('#start_date').data("DateTimePicker").maxDate(e.date);
            });
        }

        if($("#display_map").length == 1){
            initialize();
            function initialize() {
                // Define the latitude and longitude positions
                var latitude = parseFloat(document.getElementById('latitude').value); // Latitude get from above variable
                var longitude = parseFloat(document.getElementById('longitude').value); // Longitude from same
                var latlngPos = new google.maps.LatLng(latitude, longitude);

                

                // Set up options for the Google map
                var myOptions = {
                    zoom: 15,
                    center: latlngPos,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControlOptions: true,
                    zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE
                    }
                };
                // Define the map
                map = new google.maps.Map(document.getElementById("display_map"), myOptions);

                // Add the marker
                var marker = new google.maps.Marker({
                    position: latlngPos,
                    map: map,
                    draggable: true,
                });

                addMarker(latlngPos, 'Default Marker', map);
      
                google.maps.event.addListener(map, 'dragstart', function(event) {
                    //infowindow.open(map,marker);
                    //addMarker(event.latlngPos, 'Click Generated Marker', map);     
                    //var lat, lng, address;
                });

                google.maps.event.addListener(marker,'dragend',function(event) {
                    document.getElementById('latitude').value = event.latLng.lat();
                    document.getElementById('longitude').value = event.latLng.lng();
                    //alert(marker.getPosition());
                });
                
                var input = document.getElementById('search_address');
                var searchBox = new google.maps.places.SearchBox(input);
                //console.log(searchBox);
                //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.addListener('bounds_changed', function() {
                  searchBox.setBounds(map.getBounds());
                });
                var markers = [];
                searchBox.addListener('places_changed', function() {
                    
                    var places = searchBox.getPlaces();
                    if (places.length == 0) {
                        return;
                    }
                    markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                    markers = [];
                    var bounds = new google.maps.LatLngBounds();
                    places.forEach(function(place) {
                        if (!place.geometry) {
                          console.log("Returned place contains no geometry");
                          return;
                        }
                        //console.log(place.geometry.location.lng());
                        var icon = {
                          size: new google.maps.Size(71, 71),
                          origin: new google.maps.Point(0, 0),
                          anchor: new google.maps.Point(17, 34),
                          scaledSize: new google.maps.Size(25, 25)
                        };
                        markers.push(new google.maps.Marker({
                            map: map,
                            // icon: icon,
                            // title: place.name,
                            position: place.geometry.location,
                            draggable: true,
                            zoom: 13
                        }));
                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }
                        document.getElementById('latitude').value = place.geometry.location.lat();
                        document.getElementById('longitude').value = place.geometry.location.lng();
                    });
                    map.fitBounds(bounds);
                });
                
            }

            function addMarker(latlng,title,map) {
                var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        title: title,
                        icon:'map-red.png',
                        draggable:true,
                        animation: google.maps.Animation.DROP
                });

                google.maps.event.addListener(marker,'drag',function(event) {
                    document.getElementById('latitude').value = event.latLng.lat();
                    document.getElementById('longitude').value= event.latLng.lng();
                });

                google.maps.event.addListener(marker,'dragend',function(event) {
                    //alert("completed");
                    document.getElementById('latitude').value = event.latLng.lat();
                    document.getElementById('longitude').value = event.latLng.lng();
                    // alert(marker.getPosition());
                });
                google.maps.event.addListener(map, 'zoom_changed', function () {
                    // document.getElementById('zoom').value =map.getZoom();
                });
        
            }
            //        
            google.maps.event.addDomListener(window, 'load', initialize);

        }
        //
        if($("#offer_type").length == 1){
           $("#offer_type").on('change', function(){
                $type = $(this).val();
                if($type == 'general'){
                    $(".date_section").show();
                    $(".notify_save").show();
                }else if($type == 'birthday'){
                    $(".date_section").hide();
                    $(".notify_save").hide();
                }else{
                    $(".date_section").hide();
                }
            });
        }

        $(document).ready(function () {
        if($("#offer_valid_in").length == 1){
            $("#offer_valid_in").on('change', function(){
                $offer_valid_in = $(this).val();
                if($offer_valid_in == 'zones'){
                    $("#zone_section").show();
                    $("#zone_section").find('select').attr('required',true);
                    $("#partner_section").hide();
                }else if($offer_valid_in == 'partners'){
                    $("#zone_section").hide();
                    $("#partner_section").show();
                    $("#partner_section").find('select').attr('required',true);
                }else{
                    $("#zone_section").hide();
                    $("#partner_section").hide();
                }
            });
        }
    });
        
        $(document).on('keyup keypress', 'form input[name="search_address"]', function(e) {
            if(e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
        $('.datepick').on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        if($('.reset_password').length > 0){
            $('.reset_password').on('click', function(){
                $("#reset_partner_id").val($(this).attr('id'));
                $(".partnername").text($(this).data('partnername'));
                $("#partnerModal").modal('show');
            });

            $('.change_password').on('click', function(){
                $("input").removeClass('error');
                $new_password = $("input[name=new_password]").val();
                $confirm_password = $("input[name=confirm_password]").val();
                
                if($new_password.trim() == ''){
                    $("input[name=new_password]").addClass('error');
                    $("input[name=new_password]").attr("placeholder", "Please type new password");
                }
                if($confirm_password.trim() == ''){
                    $("input[name=confirm_password]").addClass('error');
                    $("input[name=confirm_password]").attr("placeholder", "Please retype new password");
                }
                if($new_password.trim() != '' && $confirm_password.trim() != ''){
                    if($new_password.trim() != $confirm_password.trim()){
                        $("input[name=confirm_password]").addClass('error');
                        $("input[name=confirm_password]").val('');
                        $("input[name=confirm_password]").attr("placeholder", "Password doesnot match with above");
                    }else{
                        // alert('everything ok');
                        $form_data = $("#reset_password_form").serialize();
                        $.ajax({
                            type: 'POST',
                            url: site_url+'/admin/reset_password',
                            data: $form_data,
                            dataType: 'json',
                            success:function(response){
                                $(".partnername").text('');
                                $("#reset_partner_id").val('');
                                $("input[name=new_password]").val('');
                                $("input[name=confirm_password]").val('');
                                $("#partnerModal").modal('hide');
                                if(response.status){
                                    $("#partnerResetModal").find('.modal-body').html('Your password is successfully changed and sent to email');                                    
                                }else{
                                    $("#partnerResetModal").find('.modal-body').html('Your password couldnot be changed, please try later');
                                }
                                $("#partnerResetModal").modal('show');
                            }
                        });
                    }
                    //reset_password_form
                }
            });

            $('.cancel_change').on('click', function(){
                $(".partnername").text('');
                $("#reset_partner_id").val('');
                $("#partnerModal").modal('hide'); 
            });
        }
    });

///
