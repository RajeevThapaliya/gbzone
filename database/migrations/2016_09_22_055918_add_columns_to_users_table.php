<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('contact_person')->nullable()->default(null);
			$table->string('designation')->nullable()->default(null);
			$table->string('address')->nullable()->default(null);
			$table->string('city')->nullable()->default(null);
			$table->string('phone')->nullable()->default(null);
			$table->string('birthday')->nullable()->default(null);
			$table->string('qr_code')->nullable()->default(null);
			$table->integer('zone_id')->nullable()->default(null);
			$table->string('type');
			$table->string('is_facebook')->nullable()->default(null);
			$table->string('logo')->nullable()->default(null);
			$table->string('background_image')->nullable()->default(null);
			$table->string('latitude')->nullable()->default(null);
			$table->string('longitude')->nullable()->default(null);
			$table->softDeletes();
			$table->string('created_by');
			$table->string('updated_by');
			$table->string('deleted_by')->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('contact_person');
			$table->dropColumn('designation');
			$table->dropColumn('address');
			$table->dropColumn('city');
			$table->dropColumn('phone');
			$table->dropColumn('zone_id');
			$table->dropColumn('type');
			$table->dropColumn('is_facebook');
			$table->dropColumn('created_by');
			$table->dropColumn('updated_by');
			$table->dropColumn('deleted_at');
			$table->dropColumn('deleted_by');			
		});
	}

}
