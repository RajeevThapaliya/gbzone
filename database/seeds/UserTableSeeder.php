<?php

use Illuminate\Database\Seeder;
use App\Model\User as User; // to use Eloquent Model

class UserTableSeeder extends Seeder {

    
	public function run()
	{
		// clear table
        User::truncate();
		$users=[
                ['name'=>'admin',
				'email'=>'admin@gmail.com',
				'password'=>\Hash::make('admin'),
				'type'=>'admin',
				'created_by'=>1,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_by'=>1,
				'updated_at'=>date('Y-m-d H:i:s')]
                ];
            foreach($users as $user){
                     User::create($user);
            }        
    }
}
