<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Partner_brands extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletes;
   	protected $table = 'user_brands'; 
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','brand_id','created_by','updated_by'];

	public function user(){
         
          return $this->belongsTo('App\Model\User','user_id','id');
	}

	public function brand(){
         
          return $this->belongsTo('App\Model\Brands','brand_id','id');
	}

}
