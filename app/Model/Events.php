<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Events extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletes;
   	protected $table = 'events'; 
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','image', 'type','start_date','end_date','city','location','latitude','longitude','description','invite_to','created_by','updated_by'];


}
