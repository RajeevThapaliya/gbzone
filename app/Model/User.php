<?php namespace App\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	use SoftDeletes;
	protected $table = 'users';

	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','designation','birthday','qr_code', 'phone','created_by','updated_by','deleted_by'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	 

	public function create_by(){
         
          return $this->belongsTo('App\Model\User','created_by','id');
	}

	public function update_by(){
         
          return $this->belongsTo('App\Model\User','updated_by','id');
	} 

	public function delete_by(){
         
          return $this->belongsTo('App\Model\User','deleted_by','id');
	} 

}
