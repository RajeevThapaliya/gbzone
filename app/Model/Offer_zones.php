<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Offer_zones extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletes;
   	protected $table = 'offer_zones'; 
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['offer_id','zone_id','created_by','updated_by'];


	public function zone(){
         
          return $this->belongsTo('App\Model\Zones','zone_id','id');
	}

	public function offer(){
         
          return $this->belongsTo('App\Model\Offers','offer_id','id');
	}

}
