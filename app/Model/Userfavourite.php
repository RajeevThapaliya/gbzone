<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Userfavourite extends Model {

	protected $table = 'userfavourites';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','partners','zones'];

}
