<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Redeems extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletes;
   	protected $table = 'redeemed_offers'; 
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['offer_id','offer_partner','offer_user','redeem_code','created_by','updated_by'];

	public function offer(){
         
          return $this->belongsTo('App\Model\Offers','offer_id','id');
	}

	public function partner(){
         
          return $this->belongsTo('App\Model\Partners','offer_partner','id');
	}
	public function user(){
	         
	          return $this->belongsTo('App\Model\User','offer_user','id');
		}
}
