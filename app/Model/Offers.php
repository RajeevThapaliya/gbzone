<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Offers extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletes;
   	protected $table = 'offers'; 
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['type','title','image', 'start_date','end_date','description','created_by','updated_by'];

	public function creator(){
         
          return $this->belongsTo('App\Model\User','created_by','id');
	}
	public function modifier(){
         
          return $this->belongsTo('App\Model\User','updated_by','id');
	}
}
