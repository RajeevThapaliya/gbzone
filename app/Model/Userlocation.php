<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Userlocation extends Model {

   	protected $table = 'user_locations'; 

   	protected $fillable = ['user_id','longitude','latitude','created_by','updated_by'];

   	public function user(){
         
          return $this->belongsTo('App\Model\User','user_id','id');
	}

}
