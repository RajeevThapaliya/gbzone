<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Notifications extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletes;
   	protected $table = 'user_notifications'; 
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','push_id','device_id','platform','created_by','updated_by'];

	public function offer(){
         
          return $this->belongsTo('App\Model\Offers','offer_id','id');
	}

	public function partner(){
         
          return $this->belongsTo('App\Model\Partners','offer_partner','id');
	}
}
