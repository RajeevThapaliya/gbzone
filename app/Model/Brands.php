<?php namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Brands extends Model {
/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	use SoftDeletes;
   	protected $table = 'brands'; 
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','image','created_by','updated_by'];


}
