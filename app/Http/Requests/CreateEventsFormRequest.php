<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateEventsFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'=>'required',
			'type'=>'required',
			'start_date'=>'required',
			'end_date'=>'required',
			'location'=>'required',
			'city' => 'required',
			'description'=>'required',
			'invite_to'=>'required'
		];
	}

	public function messages()
	{
		return [
				 'name.required' => 'Event name is required',
				 'type.required' => 'Type is required',
				 'start_date.required' => 'Start date is required',
				 'end_date.required' => 'End date is required',
				 'location.required' => 'Location is required',
				 'city.required' => 'City is required',
				 'description.required' => 'Description is required',
				 'invite_to.required' => 'Invite to is required'
		];
	}
	
	
}
