<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateOffersFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'type'=>'required',
			'title'=>'required',
			'description'=>'required',
		];
	}

	public function messages()
	{
		return [
				 'type.required' => 'Type is required',
				 'title.required' => 'Title is required',
				 'description.required' => 'Description is required',
		];
	}
	
	
}
