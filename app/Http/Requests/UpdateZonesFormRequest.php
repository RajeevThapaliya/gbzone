<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\Request as Req;

class UpdateZonesFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(Req $request)
	{
		return [
			'name'=>'required|unique:zones,name,'.$request->input('id'),
			'district'=>'required',
			'radius'=>'required'
		];
	}

	public function messages()
	{
		return [
				 'name.required' => 'Zone name is required',
				 'district.required' => 'District name is required',
				 'radius.required'=>'Radius is required'
		];
	}
	
	
}
