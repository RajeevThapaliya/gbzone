<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateZonesFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'=>'required|unique:zones,name',
			'district'=>'required',
			'radius'=>'required'
		];
	}

	public function messages()
	{
		return [
				 'name.required' => 'Zone name is required',
				 'district.required' => 'District name is required',
				 'radius.required'=>'Radius is required'
		];
	}
	
	
}
