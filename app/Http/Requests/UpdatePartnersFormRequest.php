<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\Request as Req;

class UpdatePartnersFormRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(Req $request)
	{
		return [
			'name'=>'required',
			'email'=>'required|email|unique:users,email,'.$request->input('id'),
			'contact_person'=>'required',
			'designation'=>'required',
			'address'=>'required',
			'city'=>'required',
			'phone'=>'required',
			'zone_id'=>'required',
			'brand_id'=>'required'
		];
	}

	public function messages()
	{
		return [
				 'name.required' => 'Partner name is required',
				 'email.required' => 'Email is required',
				 'contact_person.required'=>'Contact Person Name is required',
				 'designation.required' =>'Designation is required',
				 'address.required'=>'Address is required',
				 'city.required' => 'City is required',
				 'phone.required'=>'Phone is required',
				 'zone_id.required'=>'Zone is required',
				 'brand_id.required' => 'Brands is required'
		];
	}
	
	
}
