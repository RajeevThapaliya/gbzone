<?php namespace App\Http\Controllers;

use App\Model\Brands;
use App\Model\Zones;
use App\Model\Partners;
use App\Model\Events;
use App\Model\Offers;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user_type = \Auth::user()->type;
		if($user_type == 'admin'){
			$brand_count = Brands::count();
			$zones_count = Zones::count();
			$partner_count = Partners::where('type','partner')->count();
			$event_count = Events::count();
			$offer_count = Offers::count();
			return view('backend/home', compact('brand_count','zones_count','partner_count','event_count','offer_count'));
		}elseif($user_type == 'partner'){
			$event_count = Events::where('created_by',\Auth::user()->id)->count();
			$offer_count = Offers::where('created_by',\Auth::user()->id)->count();
			return view('backend/home', compact('event_count','offer_count'));
		}
	}

}
