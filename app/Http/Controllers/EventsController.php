<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEventsFormRequest;

use Illuminate\Http\Request;
use App\Model\Events;
use Input;
use DB;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Model\Notifications;

class_alias(\LaravelFCM\Facades\FCM::class, 'FCM');
class_alias(\LaravelFCM\Facades\FCMGroup::class, 'FCMGroup');

class EventsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		if(\Auth::user()->type == 'admin'){
			$events = Events::orderBy('id', 'desc')->get();
		}elseif(\Auth::user()->type == 'partner'){
			$events = Events::where('created_by',\Auth::user()->id)->orderBy('id', 'desc')->get();
		}
		
		return View('backend.events.index',compact('events'));
	}

	public function get_all()
	{
		$events = Events::all();
		return response()->json(['aaData' => $events]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$today_date = date('Y-m-d');
		$event_type = array('local'=>'Local','mega'=>'Mega');
		$invite_to = array('partners'=>'Partners', 'general_users'=>'General Users','both'=>'Both');
		return View('backend.events.create', compact('event_type','invite_to','today_date'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateEventsFormRequest $request)
	{
		//
		$fileName = '';
		if (Input::file('image') && Input::file('image')->isValid()) {
			$destinationPath = 'uploads/events'; // upload path
			$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
			$fileName = rand(11111,99999).'.'.$extension; // renameing image
			Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		}
		$event = Events::create([
			'name'=>$request->input('name'),
			'type'=>$request->input('type'),
			'image'=>$fileName,
			'start_date'=>$request->input('start_date'),
			'end_date'=>$request->input('end_date'),
			'city'=>$request->input('city'),
			'location'=>$request->input('location'),
			'description'=>$request->input('description'),
			'invite_to'=>$request->input('invite_to'),
			'latitude'=>$request->input('latitude'),
			'longitude'=>$request->input('longitude'),
			'created_by'=>\Auth::user()->id,
			'updated_by'=>\Auth::user()->id
			]);
		if($event){
			$request->session()->flash('success-message', 'Succesfully Inserted');
		}else{
			$request->session()->flash('error-message', 'Insert Failed');
		}
		
		if(Input::get('save_notify')) {
	
			$optionBuiler = new OptionsBuilder();
			$optionBuiler->setTimeToLive(60*20);
			$notification_title = $request->input('name');
			$notification_body = $request->input('description');


			$notificationBuilder = new PayloadNotificationBuilder($notification_title);
			$notificationBuilder->setBody($notification_body)
			                    ->setSound('default')
			                    ->setIcon('fcm_push_icon')
			                    ->setClickAction('FCM_PLUGIN_ACTIVITY');

			$dataBuilder = new PayloadDataBuilder();
			$dataBuilder->addData(['title' => $notification_title,'body'=>$notification_body,'id'=>$event->id]);
			$option = $optionBuiler->build();
			$notification = $notificationBuilder->build();
			$data = $dataBuilder->build();

			if(isset($_GET['debug'])) {
				print_r($data);
				print_r($option);
				print_r($notification);
				die();
			}

			// You must change it to get your tokens
			
			$push_ids = Notifications::select(DB::raw('array_to_string(array_agg(DISTINCT push_id), \',\') as push_ids'))->get();
			$tokens = explode(",", $push_ids[0]->push_ids);

			
			$downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);
			 //print_r($downstreamResponse);
			echo "success count ".$downstreamResponse->numberSuccess();
			echo "failure count ".$downstreamResponse->numberFailure(); 
			echo "modifiction count ".$downstreamResponse->numberModification();

			//return Array - you must remove all this tokens in your database
			$downstreamResponse->tokensToDelete(); 

			//return Array (key : oldToken, value : new token - you must change the token in your database )
			$downstreamResponse->tokensToModify(); 

			//return Array - you should try to resend the message to the tokens in the array
			$downstreamResponse->tokensToRetry();

         
         }
		
	die('hello');
		return redirect('admin/events');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$event_info = Events::where('id',$id)->get();
		// print_r($event_info->toArray());die('hello');

		// $offer_partners = Offer_partners::with('user')->select('user_id')->where('offer_id',$id)->get();

		// $offer_zones = Offer_zones::with('zone')->select('zone_id')->where('offer_id',$id)->get();
		
		return View('backend.events.show',compact('event_info'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$today_date = date('Y-m-d');
		$event_type = array('local'=>'Local','mega'=>'Mega');
		$invite_to = array('partners'=>'Partners', 'general_users'=>'General Users','both'=>'Both');
		$event_info = Events::where('id',$id)->get();
		return View('backend.events.edit',compact('event_info','event_type','invite_to','today_date'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, CreateEventsFormRequest $request)
	{
		//
		$event_info = Events::where('id',$id)->get();
		$fileName = $event_info[0]->image;
		if (Input::file('image')) {
			$destinationPath = 'uploads/events'; // upload path
			$extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
			$fileName = rand(11111,99999).'.'.$extension; // renameing image
			Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
			@unlink('uploads/events/'.$event_info[0]->image);
		}
		$event = Events::where('id',$id)->update([
			'name'=>$request->input('name'),
			'type'=>$request->input('type'),
			'image'=>$fileName,
			'start_date'=>$request->input('start_date'),
			'end_date'=>$request->input('end_date'),
			'city'=>$request->input('city'),
			'location'=>$request->input('location'),
			'description'=>$request->input('description'),
			'invite_to'=>$request->input('invite_to'),
			'latitude'=>$request->input('latitude'),
			'longitude'=>$request->input('longitude'),
			'updated_by'=>\Auth::user()->id
			]);
		if($event){
			$request->session()->flash('success-message', 'Succesfully Updated');
		}else{
			$request->session()->flash('error-message', 'Update Failed');
		}
		return redirect('admin/events');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id,Request $request)
	{
		//
		Events::where('id',$id)->update([
			'deleted_by'=>\Auth::user()->id
			]);

		$result= Events::destroy($id);
		if($result){
			$request->session()->flash('success-message', 'Succesfully Deleted');
		}else{
			$request->session()->flash('error-message', 'Delete Failed');
		}
		return redirect('admin/events');
	}

	public function nearbyevents(Request $request)
	{
		$per_page = 5;
		$latitude = $request->input('lat');
		$longitude = $request->input('lng');
		$offset = ($request->input('offset'))?$request->input('offset'):0;
		$required_offset = ($offset>0)?(($offset-1)*$per_page):$offset;

		$results = DB::select('SELECT * FROM (SELECT id,name, image, city,location,start_date, end_date, latitude, longitude, (3959 * acos(cos(radians(:lat :: FLOAT)) * cos(radians(latitude :: FLOAT)) * cos(radians(longitude :: FLOAT) - radians(:lng :: FLOAT)) + sin(radians(:lat :: FLOAT)) * sin(radians(latitude :: FLOAT)))) * 1.6 AS distance_in_km, DATE(end_date) - DATE(CURRENT_DATE) as datediff FROM events WHERE deleted_at IS NULL) AS tbl where datediff >= 0 order by distance_in_km ASC OFFSET :offset LIMIT :per_page', ['lat'=>$latitude,'lng'=>$longitude, 'offset'=>$required_offset, 'per_page'=>$per_page]);
		return response()->json(["events"=>$results, "status"=>1], 201);
	}

	public function view_event($id)
	{
		$event_info = Events::where('id',$id)->get();

		return response()->json(["event_info"=>$event_info[0], "status"=>1], 201);
	}

	public function search_events(Request $request)
	{
		$per_page = 5;
		$keywords = strtolower($request->keywords);

		$offset = ($request->input('offset'))?$request->input('offset'):0;
		$required_offset = ($offset>0)?(($offset-1)*$per_page):$offset;

		$events = Events::where(function ($query) use ($keywords){
			                $query->where('name', 'ilike', '%' . $keywords . '%')
			                		->orWhere('city', 'ilike', '%' . $keywords . '%')
			                		->orWhere('location', 'ilike', '%' . $keywords . '%');
			            })
                		->where(DB::raw('DATE(end_date) - DATE(CURRENT_DATE)'), '>=', 0)
						->offset($required_offset)
                		->limit($per_page)
                		->get();
        
		return response()->json(['events'=>$events,"status"=>1], 200);

	}

}
