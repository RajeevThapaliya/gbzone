<?php namespace App\Http\Controllers;
use App\Model\User as User;
use App\Model\Members;
use App\Model\Notifications;
use App\Model\Userlocation;
use App\Model\Userfavourite;
use App\Model\Partners;
use App\Model\Zones;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMemberFormRequest;
//use App\Http\Requests\UpdateUserFormRequest;
use App\Http\Requests\ChangePasswordFormRequest;

use Illuminate\Http\Request;
use DB;
use Validator;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class_alias(\LaravelFCM\Facades\FCM::class, 'FCM');
class_alias(\LaravelFCM\Facades\FCMGroup::class, 'FCMGroup');

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), ['name' => 'required|max:100', 'email' => 'required|email', 'birthday' => 'required']);
		if ($validator->fails()) {
			$messages = $validator->errors();
			$error_message = array();
			foreach ($messages->all() as $key => $message) {
				$error_message[$key] = $message;
			}
			return response()->json(["msg"=>implode("@", $error_message), "status"=>0], 201);
		}

		$name = $request->input('name');
		$email = $request->input('email');
		$birthday = $request->input('birthday');
		$phone = $request->input('phone');
		$address = $request->input('address');

		$emailValidator = Validator::make($request->all(), ['email'=>'unique:users']);
		if($emailValidator->fails()){
			$user_info = Members::where('email', $request->input('email'))->get();
			Members::where('id',$user_info[0]->id)->update([
					'name'=>$name,
					'birthday'=>$birthday,
					'phone'=>$phone,
					'address'=>$address,
					'updated_by'=>1	
					]);

			if($request->input('mobile_id') != "" && $request->input('push_id') != ""){
				$notification_info = Notifications::where('device_id',$request->input('mobile_id'))->get();
				if(count($notification_info) == 0){
					Notifications::create([
						'user_id'=>$user_info[0]->id,
						'device_id'=>$request->input('mobile_id'),
						'push_id'=>$request->input('push_id'),
						'platform'=>$request->input('platform'),
						'created_by'=>$user_info[0]->id,
						'updated_by'=>$user_info[0]->id
						]);
				}else{
					Notifications::where('device_id',$request->input('mobile_id'))->update([
						'user_id'=>$user_info[0]->id,
						'push_id'=>$request->input('push_id'),
						'platform'=>$request->input('platform'),
						'updated_by'=>$user_info[0]->id
						]);
				}
			}
			return response()->json(["msg"=>"Already registered","user_id"=>$user_info[0]->id, "status"=>1], 201);
		}
		
		DB::beginTransaction();

		try{
			$user = Members::create([
				'name'=>$name,
				'email'=>$email,
				'birthday'=>$birthday,
				'phone'=>$phone,
				'address'=>$address,
				'type'=>'member',
				'is_facebook'=>1,
				'created_by'=>1,
				'updated_by'=>1
				]);
			$notification = Notifications::create([
				'user_id'=>$user->id,
				'device_id'=>$request->input('mobile_id'),
				'push_id'=>$request->input('push_id'),
				'platform'=>$request->input('platform'),
				'created_by'=>$user->id,
				'updated_by'=>$user->id
				]);

			DB::commit();
			$request->session()->flash('success-message', 'Succesfully Inserted');
			return response()->json(["user_id"=>$user->id, "msg"=>"Registration successfully", "status"=>1], 201);
		} catch (\Exception $e) {
			DB::rollback();

			$request->session()->flash('error-message', 'Insert Failed');
			return response()->json(["msg"=>$e, "status"=>0], 201);
			// something went wrong
		}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user_info = User::where('id',$id)
					->get();
					
		return View('backend.user.show',compact('user_info'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,UpdateUserFormRequest $request)
	{

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}

	public function changepassword()
	{
		
		return View('backend.user.changepassword');
	}

	public function updatepassword(ChangePasswordFormRequest $request)
	{
		
        $user = \Auth::user();
		
		//print_r($user);
		
		if (!\Hash::check($request->input('old_password'), $user->password)) {
			return redirect('admin/changepassword')->withErrors([
						'old_password' => 'Old password did\'nt matched with user password.'
					]);
		} else {
			
			$user->password =\Hash::make($request->input('password'));
			$user->save();

			
			return redirect('/');
		}
		
	}
	public function resetpassword($id)
	{

	}

	public function newpassword(Request $request)
	{
		$password = $this->create_password(8);
		$email = $request->input('email');
		if($email){

			$user_info = Members::where('email', $request->input('email'))->get();

			//print_r($user_info);die;
		
			if(!$user_info->isEmpty()){
		

				Members::where('id',$user_info[0]->id)->update([
						'password'=>\Hash::make($password)	
						]);
				$subject = 'User Credentials!';
				$message = "<html>
							    <body>
							    <p>Hi {$request->input('name')}</p>
							    <p>You have been registered in GB Web Application.Your login credential for is as below:</p>
								<span>Username: {$request->input('email')}</span><br/>
								<span>Password: {$password}</span>
							    </body>
							</html>";
				$headers = 'From: info@gbzone.ml' . "\r\n";
				$headers .='Reply-To:info@gbzone.ml' . "\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
		        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		        $headers .= "X-Priority: 3\r\n";
		        $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
				$mail=mail($request->input('email'), $subject, $message, $headers);
				if($mail){
					$request->session()->flash('success-message', 'Password for login to application has been sent to your email address.');
				}else{
					$request->session()->flash('error-message', 'Unable to send email');
				}

			}else{

				$request->session()->flash('error-message', 'Please enter correct email address.');
			}
		}else{
			$request->session()->flash('error-message', 'Please enter correct email address.');

		}
		return redirect(asset('auth/login'));
				
	}

	function create_password($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

	public function notify()
	{
                date_default_timezone_set('Asia/Kathmandu');
		// DB::enableQueryLog();
		$birthday_users = Members::select(DB::raw('array_to_string(array_agg(DISTINCT id), \',\') as user_ids'))
									->where(DB::raw('to_char(birthday::DATE, \'MM-DD\')'),DB::raw('to_char(DATE(CURRENT_DATE), \'MM-DD\')'))
									->where('type','=','member')
									->get();
			// print_r(DB::getQueryLog());die;
		if($birthday_users[0]->user_ids != ""){
			//send notification to birthday users
			$optionBuiler = new OptionsBuilder();
			$optionBuiler->setTimeToLive(60*20);
			$notification_title = 'HAPPY BIRTHDAY';
			$notification_body = 'Enjoy Birthday With GB';


			$notificationBuilder = new PayloadNotificationBuilder($notification_title);
			$notificationBuilder->setBody($notification_body)
			                    ->setSound('default')
			                    ->setIcon('fcm_push_icon')
			                    ->setClickAction('FCM_PLUGIN_ACTIVITY');

			$dataBuilder = new PayloadDataBuilder();
			$dataBuilder->addData(['title' => $notification_title,'body'=>$notification_body,'type'=>'birthday']);
			$option = $optionBuiler->build();
			$notification = $notificationBuilder->build();
			$data = $dataBuilder->build();

			if(isset($_GET['debug'])) {
				print_r($data);
				print_r($option);
				print_r($notification);
				die();
			}

			// You must change it to get your tokens
			$ar_birthday_users = explode(",", $birthday_users[0]->user_ids);
			$push_ids = Notifications::select(DB::raw('array_to_string(array_agg(DISTINCT push_id), \',\') as push_ids'))->whereIn('user_id', $ar_birthday_users)->get();
			$tokens = explode(",", $push_ids[0]->push_ids);

			
			$downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);
			// print_r($downstreamResponse);
			echo "success count ".$downstreamResponse->numberSuccess();
			echo "failure count ".$downstreamResponse->numberFailure(); 
			echo "modifiction count ".$downstreamResponse->numberModification();

			//return Array - you must remove all this tokens in your database
			$downstreamResponse->tokensToDelete(); 

			//return Array (key : oldToken, value : new token - you must change the token in your database )
			$downstreamResponse->tokensToModify(); 

			//return Array - you should try to resend the message to the tokens in the array
			$downstreamResponse->tokensToRetry();
		}
	}
	
	public function user_location(Request $request)
	{
		$user_id=$request->header('userid');
		$userlocation=Userlocation::where('user_id',$user_id)->get();
		$data=$request->all();

		if($userlocation->isEmpty()){
		$user_locate=Userlocation::create([
						'user_id'=>$user_id,
						'longitude'=>$data[0]['longitude'],
						'latitude'=>$data[0]['latitude']
						]);

			return response()->json(["msg"=>"user location has been saved.", "status"=>1], 200);
		}else{
			$user_locate=Userlocation::where('user_id',$user_id)->update([
						'user_id'=>$user_id,						
						'longitude'=>$data[0]['longitude'],
						'latitude'=>$data[0]['latitude']
						]);

			return response()->json(["msg"=>"user location has been updated.", "status"=>1], 200);
		}
	}

	// public function user_favourite_list(Request $request)
	// {
	// 	$user_id=$request->input('user_id');		
	// 	$userfavourite=Userfavourite::where('user_id',$user_id)->get();
	// 	if(!$userfavourite->isEmpty()){
	// 		$partners=explode(",",$userfavourite[0]->partners);			
	// 		$partners=array_filter($partners);

	// 		$zones=explode(",",$userfavourite[0]->zones);			
	// 		$zones=array_filter($zones);
	// 	}else{
	// 		$partners=array();
	// 		$zones=array();
	// 	}
	// 	return response()->json(["partners"=>$partners,"zones"=>$zones, "status"=>1], 200);

	// }

	public function user_favourite_list(Request $request)
	{
		$user_id=(int)$request->input('user_id');		
		$userfavourite=Userfavourite::where('user_id',$user_id)->get();

		if(!$userfavourite->isEmpty()){
			$partners_list=$userfavourite[0]->partners;


			if($partners_list){
				$partners=explode(",",$partners_list);
			}else{
				$partners=array();
			}
			$partner_list=array();
			$i=0;
			foreach($partners as $key=>$value){
				// $partner=Partners::where('type','partner')->where('id',(int)$value)->get();

				$partner = DB::table('users')
					->select(DB::raw('users.id,users.name,users.logo, users.background_image,users.longitude,users.latitude,array_to_string(array_agg(DISTINCT offers.title),\',\') as offer_title,array_to_string(array_agg(DISTINCT brands.image),\',\') as brand_images'))
					 ->leftJoin('offer_users','offer_users.user_id','=','users.id')
					 ->leftJoin('offers','offer_users.offer_id','=','offers.id')
					 ->leftJoin('user_brands','user_brands.user_id','=','offer_users.user_id')
					 ->leftJoin('brands','brands.id','=','user_brands.brand_id')
					 ->where('users.type','=','partner')
					 ->where('users.id','=',$value)
					 ->whereNull('users.deleted_by')
			        ->whereNull('offer_users.deleted_by')
			        ->whereNull('user_brands.deleted_by')
					->groupBy('users.id', 'users.name','users.logo','users.background_image')
					->get();
				if($partner){
					$partner_list[$i]=$partner[0];
					$i++;
				}
			}		
			// $partners=array_filter($partners);

			$zones_list=$userfavourite[0]->zones;
			if($zones_list){
				$zones=explode(",",$zones_list);
			}else{
				$zones=array();
			}
			$zone_list=array();
			$j=0;
			foreach($zones as $key=>$value){
				// $zone=Zones::where('id',(int)$value)->get();

				$zone = Zones::select(DB::raw('zones.id, zones.name,zones.latitude,zones.longitude,zones.radius, image, array_to_string(array_agg(DISTINCT users.name),\',\') as offers_at'))
						->leftJoin('users','users.zone_id','=','zones.id')
						->where('zones.id',$value)
						->where('users.type','=','partner')
						->whereNull('users.deleted_at')
						->groupBy('zones.id','zones.name')
						->get();
				if($zone){
				$zone_list[$j]=$zone[0];
				$j++;
				}
			}			
			// $zones=array_filter($zones);
		}else{
			$partner_list=array();
			$zone_list=array();
		}
		return response()->json(["partners"=>$partner_list,"zones"=>$zone_list, "status"=>1], 200);

	}

	public function user_favourites(Request $request)
	{

		$user_id=(int)$request->input('user_id');

		$type = $request->input('type');
		$status=$request->input('status');
		// if($type=='partner'){		
		// $partner_id = $request->input('favourite_id');
		// }elseif($type=='zone'){
		// 	$zone_id = $request->input('favourite_id');
		// }
		$userfavourite=Userfavourite::where('user_id',$user_id)->get();


		if($userfavourite->isEmpty()){
			Userfavourite::create([
						'user_id'=>$user_id,
						'partners'=>($type=='partner')?$request->input('favourite_id'):null,
						'zones'=>($type=='zone')?$request->input('favourite_id'):null
						]);

			return response()->json(["msg"=>"This has been added to favourite list.", "status"=>1], 200);
		}else{
			if($type=='partner'){
				$partners=explode(",",$userfavourite[0]->partners);
				$key=array_search($request->input('favourite_id'),$partners);
				if($status=='1'){
					if($key===false){					
						array_push($partners,$request->input('favourite_id'));
					}
				}else{
					if($key!==false){
					    unset($partners[$key]);
					    $partners = array_values($partners);
					}
				}				
				$partners=implode(",",array_filter($partners));

			}else{
				$partners=$userfavourite[0]->partners;
			}

			if($type=='zone'){
				$zones=explode(",",$userfavourite[0]->zones);
				$key=array_search($request->input('favourite_id'),$zones);
								
				if($status=='1'){					
					if($key===false){
						array_push($zones,$request->input('favourite_id'));
					}
				}else{
					if($key!==false){
					    unset($zones[$key]);
					    $zones = array_values($zones);
					}
				}					
				$zones=implode(",",array_filter($zones));
			}else{
				$zones=$userfavourite[0]->zones;
			}

			Userfavourite::where('user_id',$user_id)->update([
						'user_id'=>$user_id,						
						'partners'=>$partners,
						'zones'=>$zones
						]);

			return response()->json(["msg"=>"This has been added to favourite list.", "status"=>1], 200);
		}
	}

}
