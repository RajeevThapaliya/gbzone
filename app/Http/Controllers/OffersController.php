<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOffersFormRequest;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Model\Offers;
use App\Model\Zones;
use App\Model\Partners;
use App\Model\Offer_partners;
use App\Model\Offer_zones;
use DB;
use Input;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Model\Notifications;

class_alias(\LaravelFCM\Facades\FCM::class, 'FCM');
class_alias(\LaravelFCM\Facades\FCMGroup::class, 'FCMGroup');

class OffersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		
		// DB::enableQueryLog();
		$data['type']=null;
		$data['upcomming_expired']=null;
		if(\Auth::user()->type == 'admin'){
			$offers = Offers::orderBy('id', 'desc');
		}elseif(\Auth::user()->type == 'partner'){
			$offers = Offers::where('created_by',\Auth::user()->id)->orderBy('id', 'desc');
		}
		if($request->exists('type') && $request->type!=''){

			$offers=$offers->where('type',$request->type);
			$data['type']=$request->type;
		}
		if($request->exists('upcomming_expired') && $request->upcomming_expired!=''){
		
			if($request->upcomming_expired=='upcomming'){
				$offers=$offers->whereDate('start_date','>',Carbon::today()->toDateString());
				$data['upcomming_expired']=$request->upcomming_expired;
			}
			
			if($request->upcomming_expired=='expired'){
				$offers=$offers->whereDate('end_date','<',Carbon::today()->toDateString());
				$data['upcomming_expired']=$request->upcomming_expired;
			}
		}

		$offers=$offers->get();
		// print_r(DB::getQueryLog());die('hello');
		// print_r($offers->toArray());die('hello');
		return View('backend.offers.index', compact('offers','data'));
	}

	public function get_all()
	{
		$offers = Offers::all();
		return response()->json(['aaData' => $offers]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$offer_type = array('general'=>'General','birthday'=>'Birthday');
		$offer_valid_in = array('zones'=>'GB Zone Wise','partners'=>'Partner Wise');
		$zones=Zones::get()->lists('name', 'id');
		$partners=Partners::where('type','partner')->get()->lists('name', 'id');
		return View('backend.offers.create', compact('offer_type','offer_valid_in','zones','partners'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateOffersFormRequest $request)
	{
		//
		// print_r($request->all());die;
		$offer_datas = array();
		$offer_datas['image'] ='';
		if ($request->file('image') && $request->file('image')->isValid()) {
			$destinationPath = 'uploads/offers'; // upload path
			$extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
			$offer_datas['image'] = rand(11111,99999).'.'.$extension; // renameing image
			$request->file('image')->move($destinationPath, $offer_datas['image']); // uploading file to given path
		}

		
		$offer_datas['type'] = $request->input('type');
		$offer_datas['title'] = $request->input('title');
		$offer_datas['description'] = $request->input('description');
		$offer_datas['created_by'] = \Auth::user()->id;
		$offer_datas['updated_by'] = \Auth::user()->id;

		if($request->input('type') == 'general'){
			$offer_datas['start_date'] = $request->input('start_date');
			$offer_datas['end_date'] = $request->input('end_date');
		}

		DB::beginTransaction();

		try{
			$offer = Offers::create($offer_datas);
			$offer_id = $offer->id;
			if(\Auth::user()->type == 'admin'){
		
				if($request->input('offer_valid_in') == 'zones'){
					foreach($request->input('zone_ids') as $key => $val){
						$offer_zone = Offer_zones::create([
							'offer_id'=>$offer_id,
							'zone_id' => $val,
							'created_by'=>\Auth::user()->id,
							'updated_by'=>\Auth::user()->id
							]);
					}
				}elseif($request->input('offer_valid_in') == 'partners'){
					foreach($request->input('partner_ids') as $key => $val){
						$offer_partner = Offer_partners::create([
							'offer_id'=>$offer_id,
							'user_id' => $val,
							'created_by'=>\Auth::user()->id,
							'updated_by'=>\Auth::user()->id
							]);
					}
				}
			}elseif(\Auth::user()->type == 'partner'){
				$offer_partner = Offer_partners::create([
							'offer_id'=>$offer_id,
							'user_id' =>\Auth::user()->id,
							'created_by'=>\Auth::user()->id,
							'updated_by'=>\Auth::user()->id
							]);
			}
			DB::commit();
			$request->session()->flash('success-message', 'Succesfully Inserted');
		} catch (\Exception $e) {
			DB::rollback();
			// something went wrong
			$request->session()->flash('error-message', 'Insert Failed');
		}
		
		if(Input::get('save_notify')) {
			$optionBuiler = new OptionsBuilder();
			$optionBuiler->setTimeToLive(60*20);
			$notification_title = $request->input('title');
			$notification_body = $request->input('description');


			$notificationBuilder = new PayloadNotificationBuilder($notification_title);
			$notificationBuilder->setBody($notification_body)
			                    ->setSound('default')
			                    ->setIcon('fcm_push_icon')
			                    ->setClickAction('FCM_PLUGIN_ACTIVITY');

			$dataBuilder = new PayloadDataBuilder();
			$dataBuilder->addData(['title' => $notification_title,'body'=>$notification_body,'type'=>'offer','id'=>$offer_id]);
			$option = $optionBuiler->build();
			$notification = $notificationBuilder->build();
			$data = $dataBuilder->build();

			if(isset($_GET['debug'])) {
				print_r($data);
				print_r($option);
				print_r($notification);
				die();
			}

			// You must change it to get your tokens
			
			$push_ids = Notifications::select(DB::raw('array_to_string(array_agg(DISTINCT push_id), \',\') as push_ids'))->get();
			$tokens = explode(",", $push_ids[0]->push_ids);

			
			$downstreamResponse = FCM::sendTo($tokens, $option, $notification,$data);
			 //print_r($downstreamResponse);
			echo "success count ".$downstreamResponse->numberSuccess();
			echo "failure count ".$downstreamResponse->numberFailure(); 
			echo "modifiction count ".$downstreamResponse->numberModification();

			//return Array - you must remove all this tokens in your database
			$downstreamResponse->tokensToDelete(); 

			//return Array (key : oldToken, value : new token - you must change the token in your database )
			$downstreamResponse->tokensToModify(); 

			//return Array - you should try to resend the message to the tokens in the array
			$downstreamResponse->tokensToRetry();

         
         }		

		return redirect('admin/offers');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$offer_info = Offers::where('id',$id)->get();

		$offer_partners = Offer_partners::with('user')->select('user_id')->where('offer_id',$id)->get();

		

		// $offer_partners = Offer_partners::select(DB::raw("array_to_string(array_agg(user_id), ',') as user_ids"))->where('offer_id',$id)->get();

		// $assigned_partners = ($offer_partners[0]->user_ids != "")?explode(",", $offer_partners[0]->user_ids):array();	

		$offer_zones = Offer_zones::with('zone')->select('zone_id')->where('offer_id',$id)->get();


		// $offer_zones = Offer_zones::select(DB::raw("array_to_string(array_agg(zone_id), ',') as zone_ids"))->where('offer_id',$id)->get();
		// $assigned_zones = ($offer_zones[0]->zone_ids != "")?explode(",", $offer_zones[0]->zone_ids):array();	
		
		if(!$offer_zones->isEmpty()){
			$offer_valid_type = 'GB Zone Wise';
		}else if(!$offer_partners->isEmpty()){
			$offer_valid_type = 'Partner Wise';
		}else{
			$offer_valid_type = '';
		}

		// if($offer_zones[0]->zone_ids != ""){
		// 	$offer_valid_type = 'zones';
		// }
		// if($offer_partners[0]->user_ids != ""){
		// 	$offer_valid_type = 'partners';
		// }

		// $offer_type = array('general'=>'General','birthday'=>'Birthday');
		// $offer_valid_in = array('zones'=>'GB Zone Wise','partners'=>'Partner Wise');
		// $zones=Zones::get()->lists('name', 'id');
		// $partners=Partners::where('type','partner')->get()->lists('name', 'id');
		
		return View('backend.offers.show',compact('offer_info','offer_partners','offer_zones','offer_valid_type'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$offer_info = Offers::where('id',$id)->get();
		$offer_partners = Offer_partners::select(DB::raw("array_to_string(array_agg(DISTINCT user_id), ',') as user_ids"))->where('offer_id',$id)->get();
		$assigned_partners = ($offer_partners[0]->user_ids != "")?explode(",", $offer_partners[0]->user_ids):array();

		$offer_zones = Offer_zones::select(DB::raw("array_to_string(array_agg(DISTINCT zone_id), ',') as zone_ids"))->where('offer_id',$id)->get();
		$assigned_zones = ($offer_zones[0]->zone_ids != "")?explode(",", $offer_zones[0]->zone_ids):array();		
		
		$offer_valid_type = '';
		if($offer_zones[0]->zone_ids != ""){
			$offer_valid_type = 'zones';
		}
		if($offer_partners[0]->user_ids != ""){
			$offer_valid_type = 'partners';
		}

		$offer_type = array('general'=>'General','birthday'=>'Birthday');
		$offer_valid_in = array('zones'=>'GB Zone Wise','partners'=>'Partner Wise');
		$zones=Zones::get()->lists('name', 'id');
		$partners=Partners::where('type','partner')->get()->lists('name', 'id');
		
		return View('backend.offers.edit',compact('offer_info','offer_type','offer_valid_in','zones','partners','assigned_partners','assigned_zones','offer_valid_type'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, CreateOffersFormRequest $request)
	{
		//
		$offer_datas = array();
		$offer_datas['type'] = $request->input('type');
		$offer_datas['title'] = $request->input('title');
		$offer_datas['description'] = $request->input('description');
		$offer_datas['created_by'] = \Auth::user()->id;
		$offer_datas['updated_by'] = \Auth::user()->id;

		if($request->input('type') == 'general'){
			$offer_datas['start_date'] = $request->input('start_date');
			$offer_datas['end_date'] = $request->input('end_date');
		}

		$offer_info = Offers::where('id',$id)->get();

		$offer_datas['image'] = $offer_info[0]->image;
		if ($request->file('image') && $request->file('image')->isValid()) {
			$destinationPath = 'uploads/offers'; // upload path
			$extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
			$offer_datas['image'] = rand(11111,99999).'.'.$extension; // renameing image
			$request->file('image')->move($destinationPath, $offer_datas['image']); // uploading file to given path
			@unlink('uploads/offers/'.$offer_info[0]->image);
		}

		DB::beginTransaction();
		try{

			$offer = Offers::where('id',$id)->update($offer_datas);
			if(\Auth::user()->type == 'admin'){
				if($request->input('offer_valid_in') == 'zones'){
					Offer_zones::where('offer_id',$id)->update([
						'deleted_by'=>\Auth::user()->id,
						'deleted_at'=>date('Y-m-d H:i:s')
						]);
					foreach($request->input('zone_ids') as $key => $val){
						$offer_zone = Offer_zones::create([
							'offer_id'=>$id,
							'zone_id' => $val,
							'created_by'=>\Auth::user()->id,
							'updated_by'=>\Auth::user()->id
							]);
					}
				}elseif($request->input('offer_valid_in') == 'partners'){
					Offer_partners::where('offer_id',$id)->update([
						'deleted_by'=>\Auth::user()->id,
						'deleted_at'=>date('Y-m-d H:i:s')
						]);
					foreach($request->input('partner_ids') as $key => $val){
						$offer_partner = Offer_partners::create([
							'offer_id'=>$id,
							'user_id' => $val,
							'created_by'=>\Auth::user()->id,
							'updated_by'=>\Auth::user()->id
							]);
					}
				}
			}elseif(\Auth::user()->type == 'partner'){
				$offer_partner = Offer_partners::create([
							'offer_id'=>$offer_id,
							'user_id' =>\Auth::user()->id,
							'created_by'=>\Auth::user()->id,
							'updated_by'=>\Auth::user()->id
							]);
			}

			DB::commit();
			$request->session()->flash('success-message', 'Succesfully Updated');
		} catch (\Exception $e) {
			DB::rollback();
			// something went wrong
			$request->session()->flash('error-message', 'Update Failed');
		}
		return redirect('admin/offers');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id,Request $request)
	{
		//
		Offers::where('id',$id)->update([
			'deleted_by'=>\Auth::user()->id
			]);

		$result= Offers::destroy($id);
		$request->session()->flash('success-message', 'Succesfully deleted');
		return redirect('admin/offers');
	}

	public function view_offer($id)
	{
		$offer_info = Offers::with('creator')->where('id',$id)->get();
		
		if(!$offer_info->isEmpty()){
		
			$offer_partners = Offer_partners::select('users.id', 'users.name','users.logo')
								->leftJoin('users', 'users.id', '=', 'offer_users.user_id')
								->where('offer_users.offer_id','=', $id)
								->whereNull('users.deleted_by')
								->get();


			if(!$offer_partners->isEmpty()){
				$offer_info[0]->valid_type = 'partner';
			}else{
				$offer_partners = Offer_zones::select('zones.id', 'zones.name','zones.image')
									->leftJoin('zones', 'zones.id', '=', 'offer_zones.zone_id')
									->where('offer_zones.offer_id','=', $id)
									->whereNull('zones.deleted_by')
									->get();
				if(!$offer_partners->isEmpty())

				$offer_info[0]->valid_type = 'zone';
			}
		}
		else{
			$offer_partners = array();
			$offer_info[0]=array();
		}

		return response()->json(["offer_info"=>$offer_info[0],'offer_valid_in'=>$offer_partners, "status"=>1], 201);
	}

	public function nearbyofffers(Request $request)
	{
		$per_page = 5;
		$latitude = $request->input('lat');
		$longitude = $request->input('lng');
		// $latitude = '27.704328';
		// $longitude = '85.3051074';
		$offset = ($request->input('offset'))?$request->input('offset'):0;
		$required_offset = ($offset>0)?(($offset-1)*$per_page):$offset;

		$offer_image = "";
		if($offset == 0 || $offset == 1){
			// $random_offer = Offers::select('image')->get()->random(1);
			$random_offer = Offers::orderBy(DB::raw('RANDOM()'))->get();
			$offer_image = $random_offer[0]->image;
		}

		// $offer_partners = DB::table('offers')
		// 					->select(DB::raw('offers.id, offers.title, offers.image, array_to_string(array_agg(users.name),\',\') as offer_available, (3959 * acos(cos(radians('.$latitude.' :: FLOAT)) * cos(radians(zones.latitude :: FLOAT)) * cos(radians(zones.longitude :: FLOAT) - radians('.$longitude.' :: FLOAT)) + sin(radians('.$latitude.' :: FLOAT)) * sin(radians(zones.latitude :: FLOAT)))) * 1.6 AS distance_in_km, creator.type as created_by'))
		// 					->leftJoin('offer_users', 'offer_users.offer_id', '=', 'offers.id')
		// 					->leftJoin('users', 'users.id', '=', 'offer_users.user_id')
		// 					->leftJoin('zones', 'zones.id', '=', 'users.zone_id')
		// 					->leftJoin('users as creator','creator.id', '=', DB::raw('offers.created_by::INT'))
		// 					->where('offers.type', '=', 'general')
		// 					->whereNull('offers.deleted_by')
		// 					->orderby('distance_in_km', 'asc')
		// 					->groupBy('offers.id', 'offers.title', 'offers.image', 'zones.latitude', 'zones.longitude', 'creator.type')
		// 					->having(DB::raw('DATE(end_date) - DATE(CURRENT_DATE)'), '>=', 0)
		// 					->having(DB::raw('array_to_string(array_agg(users.name),\',\')'), '!=', '')
		// 					->get();
		
		
		// $offer_zones = DB::table('offers')
		// 					->select(DB::raw('offers.id, offers.title, offers.image, array_to_string(array_agg(users.name),\',\') as offer_available, (3959 * acos(cos(radians('.$latitude.' :: FLOAT)) * cos(radians(zones.latitude :: FLOAT)) * cos(radians(zones.longitude :: FLOAT) - radians('.$longitude.' :: FLOAT)) + sin(radians('.$latitude.' :: FLOAT)) * sin(radians(zones.latitude :: FLOAT)))) * 1.6 AS distance_in_km, creator.type as created_by'))
		// 					->leftJoin('offer_zones', 'offer_zones.offer_id', '=', 'offers.id')
		// 					->leftJoin('zones', 'zones.id', '=', 'offer_zones.zone_id')
		// 					->leftJoin('users','users.zone_id','=', 'zones.id')
		// 					->leftJoin('users as creator','creator.id', '=', DB::raw('offers.created_by::INT'))
		// 					->where('offers.type', '=', 'general')
		// 					->whereNull('offers.deleted_by')
		// 					->orderby('distance_in_km', 'asc')
		// 					->groupBy('offers.id', 'offers.title','offers.image', 'zones.latitude', 'zones.longitude', 'creator.type')
		// 					->having(DB::raw('DATE(end_date) - DATE(CURRENT_DATE)'), '>=', 0)
		// 					->having(DB::raw('array_to_string(array_agg(zones.name),\',\')'), '!=', '')
		// 					->get();

		
		$offer_partners = DB::select('select offers.id, offers.title, offers.image, creator.type, \'partner\' as offer_assigned_to, min(tbl.distance_in_km) as distance_in_km, array_to_string(array_agg(DISTINCT users.name), \',\') as offer_available from offer_users
			  LEFT JOIN offers ON (offers.id = offer_users.offer_id)
			  LEFT JOIN users ON (users.id = offer_users.user_id)
			  LEFT JOIN zones on (zones.id = users.zone_id)
			  LEFT JOIN (SELECT id,name,image,latitude,longitude,radius,( 3959 * acos( cos(radians('.$latitude.' :: FLOAT)) * cos(radians(latitude :: FLOAT)) * cos( radians(longitude :: FLOAT) - radians('.$longitude.' :: FLOAT)) + sin(radians('.$latitude.' :: FLOAT)) * sin(radians(latitude :: FLOAT)))) * 1.6 AS distance_in_km FROM zones WHERE deleted_by IS NULL) as tbl on (tbl.id=users.zone_id)
			  LEFT JOIN users as creator on (creator.id = offers.created_by::INT)
			  where offers.deleted_by IS NULL AND users.deleted_by IS NULL AND  (DATE(end_date) - DATE(CURRENT_DATE)) >= 0

			  group by offers.id, offers.title, offers.image, creator.type

			  order by distance_in_km');

		// print_r(DB::getQueryLog());






		$offer_zones = DB::select('select offers.id, offers.title, offers.image, users.type, \'zone\' as offer_assigned_to, min(tbl.distance_in_km) as distance_in_km, array_to_string(array_agg(DISTINCT tbl.name), \',\') as offer_available from offer_zones

  LEFT JOIN offers ON (offers.id = offer_zones.offer_id)

  LEFT JOIN (SELECT id, name, image, latitude, longitude, radius, ( 3959 * acos( cos(radians('.$latitude.' :: FLOAT)) * cos(radians(latitude :: FLOAT)) * cos( radians(longitude :: FLOAT) - radians('.$longitude.' :: FLOAT)) + sin(radians('.$latitude.' :: FLOAT)) * sin(radians(latitude :: FLOAT)))) * 1.6 AS distance_in_km, deleted_by FROM zones WHERE deleted_by IS NULL) as tbl on (tbl.id=offer_zones.zone_id)

  LEFT JOIN users on (users.id = offers.created_by::INT)

  where offers.deleted_by IS NULL AND tbl.deleted_by IS NULL AND (DATE(end_date) - DATE(CURRENT_DATE)) >= 0

  group by offers.id, offers.title, offers.image, users.type

  order by distance_in_km');
// var_dump($offer_zones); die();		
		$all_offers = array_merge($offer_partners, $offer_zones);

		$col  = 'distance_in_km';
		$sort = array();
		foreach ($all_offers as $i => $obj) {
		  $sort[$i] = $obj->{$col};
		}

		array_multisort($sort, SORT_ASC, $all_offers);


   		$per_page=5;
		$offset = ($request->input('offset'))?(int)$request->input('offset'):0;
		$required_offset=(($offset-1)<=0)?0:($offset-1)*$per_page;

		$all_offers = array_unique($all_offers, SORT_REGULAR);

		$all_offers = array_slice($all_offers, $required_offset, $per_page);


		return response()->json(["offer_image"=>$offer_image,"offers"=>$all_offers, "status"=>1], 201);
	}

	public function birthday_offers($user_id)
	{
		
		$offers = Offers::select('offers.id','offers.title','offers.image')
						->leftJoin('redeemed_offers', function($join) use ($user_id){
						    $join->on('offers.id', '=', 'redeemed_offers.offer_id');
						    $join->on('redeemed_offers.offer_user', '=', DB::raw("'".$user_id."'"));
						})
						->where('offers.type', '=', 'birthday')
						->where(function ($query) {
			                $query->whereNull('redeemed_offers.created_at')
			                      ->orWhere(DB::raw('date_part(\'year\',redeemed_offers.created_at)'), '<', DB::raw('date_part(\'year\', CURRENT_DATE)'));
			            })
						->get();
		return response()->json(["offers"=>$offers, "status"=>1], 201);
	}

	public function search_offers(Request $request)
	{

		$location = $request->input('location');

		$users = DB::table('users')
					->select(DB::raw('array_to_string(array_agg(id),\',\') as users'))
					->where(function ($query) use ($location){
						$query->where('address', 'ilike', '%'.$location.'%')
							  ->orWhere('city', 'ilike', '%'.$location.'%');

					})
					->whereNull('users.deleted_by')
					->get();

					
		$zones = DB::table('zones')
					->select(DB::raw('array_to_string(array_agg(id),\',\') as zones'))
					->where('name', 'ilike', '%'.$location.'%')
					->whereNull('zones.deleted_by')
					->get();

	
		DB::enableQueryLog();
		
		$offers_assigned_to_partners = array();
        if($users[0]->users != ''){
            $user_ids = explode(",", $users[0]->users);
            $offers_assigned_to_partners =DB::table('offer_users')
                            ->select(DB::raw('offer_users.offer_id as id,offers.title,offers.image,\'partner\' as offer_assigned_to,array_to_string(array_agg(DISTINCT users.name), \',\') as offer_available'))
                            ->leftJoin('offers', 'offers.id', '=', 'offer_users.offer_id')
                            ->leftJoin('users', 'users.id', '=', 'offer_users.user_id')
                            // ->leftJoin('users as creator', 'creator.id', '=','offers.created_by::INT')
                            ->whereIn('user_id', $user_ids)
                            ->where('end_date', '>=', date('Y-m-d'))
                            ->whereNull('offers.deleted_by')
                            ->whereNull('users.deleted_by')
                            ->groupBy('offer_users.offer_id','offers.title', 'offers.image')
                            ->get();
         }

        $offers_assigned_to_zones = array();
        if($zones[0]->zones != ''){
            $zone_ids = explode(",", $zones[0]->zones);
            $offers_assigned_to_zones = DB::table('offer_zones')
                            ->select(DB::raw('offers.id,offers.title,offers.image,\'zone\' as offer_assigned_to,array_to_string(array_agg(DISTINCT zones.name), \',\') as offer_available'))
                            ->leftJoin('offers', 'offers.id', '=', 'offer_zones.offer_id')                            
                            ->leftJoin('zones', 'zones.id', '=', 'offer_zones.zone_id')
                            // ->leftJoin('users', 'creator.id', '=', 'offers.created_by::INT')                            
                            ->whereIn('zone_id', $zone_ids)
                            ->where('end_date', '>=', date('Y-m-d'))
                            ->whereNull('offers.deleted_by')
                            ->whereNull('zones.deleted_by')
                            ->groupBy('offer_zones.offer_id','offers.id')
                            ->get();
        }


        // print_r($offers_assigned_to_zones);die;

		// print_r(DB::getQueryLog());die;
   
   		$per_page=5;
		$offset = ($request->input('offset'))?(int)$request->input('offset'):0;
		$required_offset=(($offset-1)<=0)?0:($offset-1)*$per_page;
		// echo $required_offset;die;

		$offers = array_merge($offers_assigned_to_zones, $offers_assigned_to_partners);
		$offers = array_slice($offers, $required_offset, $per_page);

		return response()->json(["offers"=>$offers, "status"=>1], 201);

	}

}
