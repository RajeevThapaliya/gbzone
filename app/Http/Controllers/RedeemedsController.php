<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Model\Offers;
use App\Model\Redeems;
use App\Model\Partners;
use App\Model\Offer_partners;
use DB;

class RedeemedsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		if(\Auth::user()->type == 'admin'){
			
$redeems=Redeems::with('offer','partner','user')->orderby('created_at','desc')->get();

		}elseif(\Auth::user()->type == 'partner'){
			DB::enableQueryLog();
			//$redeems=Redeems::with('offer','partner')->orderby('created_at','desc')->get();
			$redeems=Redeems::with(['offer' => function($query) {
											$query->where('created_by',\Auth::user()->id);
										},
								'partner'
								])->orderby('created_at','desc')->get();
			 //print_r(DB::getQueryLog());die('hello');
			//print_r($redeems->toArray());die('hello');
		}
		return View('backend.redeems.index',compact('redeems'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function redeem_offer(Request $request)
	{
		$offer_id = $request->input('offer_id');
		$offer_user = $request->input('offer_user');
		$partner_id = $request->input('partner_id');

		$redeem_info = Redeems::with('offer')->where('offer_id',$offer_id)->where('offer_user', $offer_user)->get();		

		if(count($redeem_info) > 0){
			
			return response()->json(["msg"=>'Already redeemed', "status"=>0], 201);
		}else{

				$valid_partner = Offer_partners::where('offer_id', $offer_id)->where('user_id', $partner_id)->get();
				if(count($valid_partner) == 0){
					return response()->json(["msg"=>"Invalid partner", "status"=>0], 201);
				}else{
					$offer=offers::where('id',$offer_id)->get();
					if($offer){
						if($offer[0]->type=="birthday"){

							$start = date('Y-m-d', strtotime('-1 year'));
							$end=date('Y-m-d');
							$redeems = Redeems::with('offer')->where('offer_user', $offer_user)->whereDate('created_at','>',$start)
								->whereDate('created_at','<=',$end)
							    ->get();
							foreach($redeems as $redeem){
								if($redeem->offer){

							  		 $dt = date("Y-m-d", strtotime($redeem->created_at));
									if($redeem->offer->type=="birthday" && $dt==$end){

										$offer_code = $this->generate_offer_code();
										$redeem = Redeems::create([
											'offer_id'=>$request->input('offer_id'),
											'offer_user'=>$request->input('offer_user'),
											'offer_partner'=>$partner_id,
											'redeem_code'=>$offer_code,
											'created_by'=>$request->input('offer_user'),
											'updated_by'=>$request->input('offer_user')
											]);
										return response()->json(["offer_code"=>$offer_code, "status"=>1], 201);
									}else{

										return response()->json(["msg"=>"Test You have already reedeem a birthday offer.", "status"=>0], 201);
									}

								}

							}
						}

						$offer_code = $this->generate_offer_code();
						$redeem = Redeems::create([
							'offer_id'=>$request->input('offer_id'),
							'offer_user'=>$request->input('offer_user'),
							'offer_partner'=>$partner_id,
							'redeem_code'=>$offer_code,
							'created_by'=>$request->input('offer_user'),
							'updated_by'=>$request->input('offer_user')
							]);
						return response()->json(["offer_code"=>$offer_code, "status"=>1], 201);
					}
				}
			}
		}
	public function user_redeem_offer(Request $request)
	{
		$per_page = 5;
		$user_id = strtolower($request->user_id);
		$offset = ($request->input('offset'))?$request->input('offset'):0;
		$required_offset = ($offset>0)?(($offset-1)*$per_page):$offset;

		$redeem_info = Redeems::with(['offer' => function($query){
												   $query->whereNull('deleted_at');
										   },'partner','user'])
						->where('offer_user',$user_id)
						->offset($required_offset)
						->limit($per_page)
						->get();
		return response()->json(['redeem_info'=>$redeem_info,"status"=>1], 200);
	}
	
	


	public function generate_offer_code()
	{
		$offer_code = rand(1000,9999);
		if (Redeems::where('redeem_code', '=', $offer_code)->exists()) {
			$this->generate_offer_code();
		}else{
		   return $offer_code;
		}
	}

}
