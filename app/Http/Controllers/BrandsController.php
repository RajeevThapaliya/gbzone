<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBrandsFormRequest;

use Illuminate\Http\Request;
use App\Model\Brands;

class BrandsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		//
		$brands=Brands::orderBy('id', 'desc')->get();
		return View('backend.brands.index',compact('brands'));
	}

	public function get_all()
	{
		$brands=Brands::all();
		return response()->json(['aaData' => $brands]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View('backend.brands.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateBrandsFormRequest $request)
	{
		//
		$fileName = '';
		if ($request->file('image') && $request->file('image')->isValid()) {
			$destinationPath = 'uploads/brands'; // upload path
			$extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
			$fileName = rand(11111,99999).'.'.$extension; // renameing image
			$request->file('image')->move($destinationPath, $fileName); // uploading file to given path
		}
		$brand = Brands::create([
			'name'=>$request->input('name'),
			'image'=>$fileName,
			'created_by'=>\Auth::user()->id,
			'updated_by'=>\Auth::user()->id
			]);
		if($brand){
			$request->session()->flash('success-message', 'Succesfully Added');
		}else{
			$request->session()->flash('error-message', 'Insert Failed');
		}

		return redirect('admin/brands');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$brand_info = Brands::where('id',$id)->get();
		// var_dump($brand_info[0]);
		return View('backend.brands.edit',compact('brand_info'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, CreateBrandsFormRequest $request)
	{
		//
		$brand_info = Brands::where('id',$id)->get();
		$fileName = $brand_info[0]->image;
		if ($request->file('image') && $request->file('image')->isValid()) {
			$destinationPath = 'uploads/brands'; // upload path
			$extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
			$fileName = rand(11111,99999).'.'.$extension; // renameing image
			$request->file('image')->move($destinationPath, $fileName); // uploading file to given path
			@unlink('uploads/brands/'.$brand_info[0]->image);
		}
		$brand = Brands::where('id',$id)->update([
			'name'=>$request->input('name'),
			'image'=>$fileName,
			'updated_by'=>\Auth::user()->id
			]);
		if($brand){
			$request->session()->flash('success-message', 'Succesfully Updated');
		}else{
			$request->session()->flash('error-message', 'Update Failed');
		}
		return redirect('admin/brands');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id,Request $request)
	{
		//
		Brands::where('id',$id)->update([
			'deleted_by'=>\Auth::user()->id
			]);

		$result= Brands::destroy($id);
		
		if($result){
			$request->session()->flash('success-message', 'Succesfully Deleted');
		}else{
			$request->session()->flash('error-message', 'Delete Failed');
		}
		return redirect('admin/brands');
	}

}
