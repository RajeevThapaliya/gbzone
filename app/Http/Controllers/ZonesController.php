<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateZonesFormRequest;
use App\Http\Requests\UpdateZonesFormRequest;

use Illuminate\Http\Request;
use App\Model\Zones;
use App\Model\Offers;
use DB;

class ZonesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$zones = Zones::orderBy('id', 'desc')->get();
		return View('backend.zones.index', compact('zones'));
	}

	public function get_all()
	{
		$zones = Zones::all();
		return response()->json(['aaData' => $zones]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View('backend.zones.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateZonesFormRequest $request)
	{
		//
		$fileName = '';
		if ($request->file('image') && $request->file('image')->isValid()) {
			$destinationPath = 'uploads/zones'; // upload path
			$extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
			$fileName = rand(11111,99999).'.'.$extension; // renameing image
			$request->file('image')->move($destinationPath, $fileName); // uploading file to given path
		}

		$zone = Zones::create([
			'name'=>$request->input('name'),
			'image'=>$fileName,
			'district'=>$request->input('district'),
			'latitude'=>$request->input('latitude'),
			'longitude'=>$request->input('longitude'),
			'radius'=>$request->input('radius'),
			'created_by'=>\Auth::user()->id,
			'updated_by'=>\Auth::user()->id
			]);
		if($zone){
			$request->session()->flash('success-message', 'Succesfully Inserted');
		}else{
			$request->session()->flash('error-message', 'Insert Failed');
		}

		return redirect('admin/zones');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$zone_info = Zones::where('id',$id)->get();
		return View('backend.zones.edit',compact('zone_info'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UpdateZonesFormRequest $request)
	{
		//
		$zone_info = Zones::where('id',$id)->get();
		$fileName = $zone_info[0]->image;
		if ($request->file('image')) {
			$destinationPath = 'uploads/zones'; // upload path
			$extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
			$fileName = rand(11111,99999).'.'.$extension; // renameing image
			$request->file('image')->move($destinationPath, $fileName); // uploading file to given path
			@unlink('uploads/zones/'.$zone_info[0]->image);
		}
		$zone = Zones::where('id',$id)->update([
			'name'=>$request->input('name'),
			'image'=>$fileName,
			'district'=>$request->input('district'),
			'latitude'=>$request->input('latitude'),
			'longitude'=>$request->input('longitude'),
			'radius'=>$request->input('radius'),
			'updated_by'=>\Auth::user()->id
			]);
		if($zone){
			$request->session()->flash('success-message', 'Succesfully Updated');
		}else{
			$request->session()->flash('error-message', 'Update Failed');
		}
		return redirect('admin/zones');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id,Request $request)
	{
		//
		Zones::where('id',$id)->update([
			'deleted_by'=>\Auth::user()->id
			]);

		$result= Zones::destroy($id);
		if($result){
			$request->session()->flash('success-message','Succesfully Deleted');
		}else{
			$request->session()->flash('error-message', 'Delete Failed');
		}
		return redirect('admin/zones');
	}

	public function nearbyzones(Request $request)
	{
		$per_page = 5;
		$latitude = $request->input('lat');
		$longitude = $request->input('lng');
		$offset = ($request->input('offset'))?$request->input('offset'):0;
		$required_offset = ($offset>0)?(($offset-1)*$per_page):$offset;

		$offers = array();
		if($offset == 0 || $offset == 1){
			$offers = Offers::select('id','title','image')->where('image','!=','')->where('type','=','general')->whereDate('end_date','>=',date('Y-m-d'))->offset(0)->limit(3)->get();
		}

		$results = DB::select('SELECT tbl.id, tbl.name, image, tbl.latitude,tbl.longitude, radius, distance_in_km, array_to_string(array_agg(DISTINCT users.name), \',\') as offers_at FROM (SELECT id,name,image, latitude, longitude, radius, (3959 * acos(cos(radians(:lat :: FLOAT)) * cos(radians(latitude :: FLOAT)) * cos(radians(longitude :: FLOAT) - radians(:lng :: FLOAT)) + sin(radians(:lat :: FLOAT)) * sin(radians(latitude :: FLOAT)))) * 1.6 AS distance_in_km FROM zones WHERE deleted_by IS NULL) AS tbl LEFT JOIN users on (users.zone_id = tbl.id AND users.deleted_by IS NULL) GROUP BY tbl.id, tbl.name, image, tbl.latitude,tbl.longitude, radius, distance_in_km order by distance_in_km ASC OFFSET :offset LIMIT :per_page', ['lat'=>$latitude,'lng'=>$longitude, 'offset'=>$required_offset, 'per_page'=>$per_page]);
		return response()->json(["offers"=>$offers, "zones"=>$results,  "status"=>1], 201);
	}

	public function search_zones(Request $request)
	{
		$per_page = 5;
		$keywords = strtolower($request->keywords);

		$offset = ($request->input('offset'))?$request->input('offset'):0;
		$required_offset = ($offset>0)?(($offset-1)*$per_page):$offset;


		// $zones = Zones::select(DB::raw('zones.id, zones.name, array_to_string(array_agg(users.name),\',\') as offers_at'))
		// 				->where(function ($query) use ($keywords) {
		// 	                $query->where('zones.name', 'ilike', '%'.$keywords.'%')
		// 	                      ->orWhere('district', 'ilike', '%' . $keywords . '%');
		// 	            })
		// 				->leftJoin('users','users.zone_id','=','zones.id')
		// 				->whereNull('users.deleted_by')
		// 				->groupBy('zones.id','zones.name')
		// 				->offset($required_offset)
  //               		->limit($per_page)
		// 				->get();
		$zones = Zones::select(DB::raw('zones.id, zones.name,zones.latitude,zones.longitude,zones.radius, image, array_to_string(array_agg(DISTINCT users.name),\',\') as offers_at'))
						->where(function ($query) use ($keywords){
							$query->where('zones.name', 'ilike', '%'.$keywords.'%')
									->orWhere('zones.district', 'ilike', '%'.$keywords.'%');
						})
						->leftJoin('users','users.zone_id','=','zones.id')
						->where('users.type','=','partner')
						->whereNull('users.deleted_at')
						->groupBy('zones.id','zones.name')
						->offset($required_offset)
						->limit($per_page)
						->get();
		return response()->json(['zones'=>$zones,"status"=>1], 200);

	}

}
