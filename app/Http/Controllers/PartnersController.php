<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePartnersFormRequest;
use App\Http\Requests\UpdatePartnersFormRequest;


use Illuminate\Http\Request;
use App\Model\Partners;
use App\Model\Brands;
use App\Model\Zones;
use App\Model\Offers;
use App\Model\Partner_brands;
use DB;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Input;


class PartnersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$partners = Partners::where('type','partner')->orderBy('id', 'desc')->get();
		return View('backend.partners.index', compact('partners'));
	}

	public function get_all()
	{
		$partners = Partners::where('type','partner')->get();
		return response()->json(['aaData' => $partners]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$brands = Brands::all();
		$zones=Zones::get()->lists('name', 'id');
		return View('backend.partners.create',compact('brands','zones'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreatePartnersFormRequest $request)
	{
		//
		// DB::beginTransaction();




		try {

		 	$logo = '';
			if ($request->file('logo') && $request->file('logo')->isValid()) {
				$destinationPath = 'uploads/partners/logo'; // upload path
				$extension = $request->file('logo')->getClientOriginalExtension(); // getting image extension
				$logo = rand(11111,99999).'.'.$extension; // renameing image
				$request->file('logo')->move($destinationPath, $logo); // uploading file to given path

			}


			$background_image = '';
			if ($request->file('background_image') && $request->file('background_image')->isValid()) {
				$destinationPath = 'uploads/partners/backgrounds'; // upload path
				$extension = $request->file('background_image')->getClientOriginalExtension(); // getting image extension
				$background_image = rand(11111,99999).'.'.$extension; // renameing image
				$request->file('background_image')->move($destinationPath, $background_image); // uploading file to given path

			}


			$password = $this->create_password(8);

			$qr_code = $this->generate_qr_code();

			DB::enableQueryLog();

			$partner = Partners::create([
				'name'=>$request->input('name'),
				'email'=>$request->input('email'),
				'password'=>\Hash::make($password),
				'contact_person'=>$request->input('contact_person'),
				'designation'=>$request->input('designation'),
				'address'=>$request->input('address'),
				'city'=>$request->input('city'),
				'phone'=>$request->input('phone'),
				'type'=>$request->input('type'),
				'zone_id'=>$request->input('zone_id'),
				'qr_code'=>$qr_code,
				'logo'=>$logo,
				'background_image'=>$background_image,
				'is_facebook'=>$request->input('facebook_link'),
				'latitude' => $request->input('latitude'),
				'longitude' => $request->input('longitude'),
				'created_by'=>\Auth::user()->id,
				'updated_by'=>\Auth::user()->id
				]);
			$partner_id = $partner->id;
					
			foreach($request->input('brand_id') as $brand_id){
				$partner_brand = Partner_brands::create([
					'user_id'=>$partner_id,
					'brand_id'=>$brand_id,
					'created_by'=>\Auth::user()->id,
					'updated_by'=>\Auth::user()->id
					]);
			}
			$subject = 'User Credentials!';
			$message = "<html>
						    <body>
						    <p>Hi {$request->input('name')}</p>
						    <p>You have been registered in GB Web Application.Your login credential for is as below:</p>
							<span>Username: {$request->input('email')}</span><br/>
							<span>Password: {$password}</span>
						    </body>
						</html>";
			$headers = 'From: info@gbzone.ml' . "\r\n";
			$headers .='Reply-To:info@gbzone.ml' . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
	        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	        $headers .= "X-Priority: 3\r\n";
	        $headers .= "X-Mailer: PHP". phpversion() ."\r\n";

			mail($request->input('email'), $subject, $message, $headers);

			DB::commit();
			$request->session()->flash('success-message', 'Succesfully Inserted');
		} catch (\Exception $e) {
			DB::rollback();
			// something went wrong
			$request->session()->flash('error-message', 'Insert Failed');
		}
		return redirect('admin/partners');
	}

	function create_password($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$partner_info = Partners::where('users.id',$id)
					->join('zones','zones.id', '=', 'users.zone_id')
					->select('users.*', 'zones.name as zone')
					->get();
					// print_r($partner_info->toArray());die('hello');
		return View('backend.partners.show',compact('partner_info'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$partner_info = Partners::where('id',$id)->get();

		// DB::enableQueryLog();
		$assigned_brands = Partner_brands::select(DB::raw("array_to_string(array_agg(DISTINCT brand_id), ',') as brand_id"))
                     ->where('user_id','=',$id)
                     ->groupBy('user_id')
                     ->get();
        // print_r(DB::getQueryLog());
        // print_r($assigned_brands[0]->brand_id); die();
        $partner_brands = ($assigned_brands && $assigned_brands[0]->brand_id != '')?explode(",", $assigned_brands[0]->brand_id):array();
        
		$brands = Brands::all();
		$zones=Zones::get()->lists('name', 'id');
		
		return View('backend.partners.edit',compact('partner_info','brands','zones','partner_brands'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UpdatePartnersFormRequest $request)
	{
		//
		DB::beginTransaction();
		try{
			$partner_info = Partners::where('id',$id)->get();

			$logo = $partner_info[0]->logo;
			if ($request->file('logo') && $request->file('logo')->isValid()) {
				$destinationPath = 'uploads/partners/logo'; // upload path
				$extension = $request->file('logo')->getClientOriginalExtension(); // getting image extension
				$logo = rand(11111,99999).'.'.$extension; // renameing image
				$request->file('logo')->move($destinationPath, $logo); // uploading file to given path
				@unlink('uploads/partners/logo/'.$partner_info[0]->logo);
			}

			$background_image = $partner_info[0]->background_image;
			if ($request->file('background_image') && $request->file('background_image')->isValid()) {
				$destinationPath = 'uploads/partners/backgrounds'; // upload path
				$extension = $request->file('background_image')->getClientOriginalExtension(); // getting image extension
				$background_image = rand(11111,99999).'.'.$extension; // renameing image
				$request->file('background_image')->move($destinationPath, $background_image); // uploading file to given path
				@unlink('uploads/partners/backgrounds/'.$partner_info[0]->background_image);
			}

			$partner = Partners::where('id',$id)->update([
				'name'=>$request->input('name'),
				'email'=>$request->input('email'),
				'contact_person'=>$request->input('contact_person'),
				'designation'=>$request->input('designation'),
				'address'=>$request->input('address'),
				'city'=>$request->input('city'),
				'phone'=>$request->input('phone'),
				'zone_id'=>$request->input('zone_id'),
				'logo'=>$logo,
				'background_image'=>$background_image,
				'is_facebook'=>$request->input('facebook_link'),
				'latitude' => $request->input('latitude'),
				'longitude' => $request->input('longitude'),
				'updated_by'=>\Auth::user()->id
				]);
			
			Partner_brands::where('user_id',$id)->update([
				'deleted_by'=>\Auth::user()->id,
				'deleted_at'=>date('Y-m-d H:i:s')
				]);

			foreach($request->input('brand_id') as $brand_id){
				$partner_brand = Partner_brands::create([
					'user_id'=>$id,
					'brand_id'=>$brand_id,
					'created_by'=>\Auth::user()->id,
					'updated_by'=>\Auth::user()->id
					]);
			}

			DB::commit();
			$request->session()->flash('success-message', 'Succesfully Updated');
		} catch (\Exception $e) {
			DB::rollback();
			// something went wrong
			$request->session()->flash('error-message', 'Update Failed');
		}
		return redirect('admin/partners');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id,Request $request)
	{
		//
		DB::beginTransaction();
		try{
			Partners::where('id',$id)->update([
				'deleted_by'=>\Auth::user()->id
				]);

			Partners::destroy($id);

			Partner_brands::where('user_id',$id)->update([
				'deleted_by'=>\Auth::user()->id
				]);

			Partner_brands::destroy($id);			

			DB::commit();
			$request->session()->flash('success-message', 'Succesfully Deleted');
		} catch (\Exception $e) {
			DB::rollback();
			// something went wrong
			$request->session()->flash('error-message', 'Delete Failed');
		}
		
		return redirect('admin/partners');
	}

	public function generate_qr_code()
	{
		$qr_code = rand(1000,9999);
		if (Partners::where('qr_code', '=', $qr_code)->exists()) {
			$this->generate_qr_code();
		}else{
		   return $qr_code;
		}
	}

	public function reset_password(Request $request)
	{
		$user_id = $request->input('id');
		$password = $request->input('new_password');
		$partner = Partners::where('id',$user_id)->update([
				'password'=>\Hash::make($password),
				'updated_by'=>\Auth::user()->id
				]);

		$partner_info = Partners::where('id',$user_id)->get();

		$subject = 'Password Reset Info';
		$message = "<html>
					    <body>
					    <p>Hi {$partner_info[0]->name}</p>
					    <p>Your password for GB Web Application has been reset successfully.Your login credential for is as below:</p>
						<span>Username: {$partner_info[0]->email}</span><br/>
						<span>Password: {$password}</span>
					    </body>
					</html>";
		$headers = 'From: info@gbzone.ml' . "\r\n";
		$headers .='Reply-To:info@gbzone.ml' . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers .= "X-Priority: 3\r\n";
        $headers .= "X-Mailer: PHP". phpversion() ."\r\n";

		mail($partner_info[0]->email, $subject, $message, $headers);
		return response()->json(["status"=>TRUE], 200);
	}

	public function get_partners($zone_id, Request $request)
	{
		$per_page = 5;
		// $offset = ($request->input('offset'))?$request->input('offset'):0;
		// $required_offset = ($offset>0)?(($offset-1)*$per_page):$offset;

		 DB::enableQueryLog();
		// $partners = DB::table('offer_users')
		// 			->select(DB::raw('users.id, users.name, users.logo, users.background_image,users.longitude,users.latitude, array_to_string(array_agg(offers.title),\',\') as offer_title, array_to_string("array_agg"(brands.image), \',\') as brand_images'))
		// 			->leftJoin('offers','offer_users.offer_id','=','offers.id')
		// 	        ->leftJoin('users', 'users.id', '=', 'offer_users.user_id')
		// 	        ->leftJoin('user_brands', 'user_brands.user_id', '=', 'users.id')
		// 	        ->leftJoin('brands', 'brands.id', '=', 'user_brands.brand_id')
		// 	        ->where('zone_id', '=', $zone_id)
		// 	        ->whereNull('users.deleted_by')
		// 	        ->whereNull('offer_users.deleted_by')
		// 	        ->whereNull('offers.deleted_by')
		// 	        ->whereNull('user_brands.deleted_by')
		// 	        ->whereNull('brands.deleted_by')
		// 	        ->groupBy('users.id', 'users.name','users.logo','users.background_image')
		// 	        // ->having(DB::raw('DATE(end_date) - DATE(CURRENT_DATE)'), '>=', 0)
		// 	        // ->offset($required_offset)->limit($per_page)
		// 	        ->get();
		  

		 $partners = DB::table('users')
					->select(DB::raw('users.id,users.name,users.logo, users.background_image,users.longitude,users.latitude,array_to_string(array_agg(DISTINCT offers.title),\',\') as offer_title,array_to_string(array_agg(DISTINCT brands.image),\',\') as brand_images'))
					 ->leftJoin('offer_users','offer_users.user_id','=','users.id')
					 ->leftJoin('offers',function ($join) {
			                $join->on('offer_users.offer_id','=','offers.id') ;
			                $join->where('offers.end_date','>',date('Y-m-d')) ;
			            })
					 // ->whereDate('offers.end_date','>',date('Y-m-d'))
					 ->leftJoin('user_brands','user_brands.user_id','=','offer_users.user_id')
					 ->leftJoin('brands','brands.id','=','user_brands.brand_id')
					 ->where('users.type','=','partner')
					 ->where('users.zone_id','=',$zone_id)
					 ->whereNull('users.deleted_by')
			        ->whereNull('offer_users.deleted_by')
			        ->whereNull('user_brands.deleted_by')			        
			        ->whereNull('offers.deleted_by')

					->groupBy('users.id', 'users.name','users.logo','users.background_image')
					->get();






		 // print_r($partners);die;
		  // print_r(DB::getQueryLog()); die();
        return response()->json(["partners"=>$partners, "status"=>1], 201);
	}


	public function details($id)
	{
		// echo $id;
		$ar_partner_info = Partners::where('id',$id)->get();
		
		$partner_info = array('id'=>$ar_partner_info[0]->id, 'name'=>$ar_partner_info[0]->name,'address'=>$ar_partner_info[0]->address,'city'=>$ar_partner_info[0]->city,'phone'=>$ar_partner_info[0]->phone,'facebook_link'=>$ar_partner_info[0]->facebook_link,'logo'=>$ar_partner_info[0]->logo,'background_image'=>$ar_partner_info[0]->background_image,'latitude'=>$ar_partner_info[0]->latitude,'longitude'=>$ar_partner_info[0]->longitude);

		$partner_brands = Partner_brands::select('image')->leftJoin('brands','brands.id','=','user_brands.brand_id')->where('user_id', $id)->get();

		// $partner_offers = DB::table('offers')->select(DB::raw('offers.id as offer_id, offers.title,users.name'))
		// 				->leftJoin('offer_users','offer_users.offer_id','=','offers.id')
		// 		        ->leftJoin('users', 'users.id', '=', 'offer_users.user_id')
		// 		        ->whereNull('offers.deleted_by')
		// 		        ->where('offer_users.user_id', '=', $id)
		// 		        ->groupBy('offers.id', 'offers.title', 'users.id', 'users.name', 'end_date')
		// 		        ->having(DB::raw('DATE(end_date) - DATE(CURRENT_DATE)'), '>=', 0)
		// 		        ->get();

		// print_r($partner_offers);die;

		// $partner_offers = DB::select('select offers.id as offer_id, offers.title as title, creator.type as creator from offers
		// 	  LEFT JOIN offer_users ON (offer_users.offer_id = offers.id)
		// 	  LEFT JOIN users ON (users.id = offer_users.user_id)
		// 	  LEFT JOIN users as creator on (creator.id = offers.created_by::INT)
		// 	  where offer_users.user_id='.$id.' AND offers.deleted_by IS NULL AND users.deleted_by IS NULL AND  (DATE(end_date) - DATE(CURRENT_DATE)) >= 0

		// 	  group by offers.id, offers.title, offers.image, creator.type');




		$partner_offers = DB::select('select offers.id as offer_id, offers.title as title, creator.type as creator from offers 
			LEFT JOIN offer_users ON (offer_users.offer_id = offers.id)
			  LEFT JOIN users ON (users.id = offer_users.user_id)
			  LEFT JOIN users as creator on (creator.id = offers.created_by::INT)
			  where offer_users.user_id='.$id.' AND offers.deleted_by IS NULL AND users.deleted_by IS NULL AND  (DATE(end_date) - DATE(CURRENT_DATE)) >= 0 group by offers.id, offers.title, offers.image, creator.type');



		// $other_offers = DB::table('offers')->select(DB::raw('offers.id as offer_id, offers.title, users.name'))
		// 				->leftJoin('offer_zones', 'offer_zones.offer_id', '=', 'offers.id')
		// 				->leftJoin('zones', 'zones.id', '=', 'offer_zones.zone_id')
		// 				->leftJoin('users', 'users.zone_id', '=', 'zones.id')
		// 				->whereNull('offers.deleted_by')
		// 		        ->where('users.id', '=', $id)
		// 		        ->groupBy('offers.id', 'offers.title', 'users.id', 'users.name', 'end_date')
		// 		        ->having(DB::raw('DATE(end_date) - DATE(CURRENT_DATE)'), '>=', 0)
		// 		        ->get();

		$other_offers = DB::select('select offers.id as offer_id, offers.title as title, creator.type as creator from offers
			  LEFT JOIN offer_zones ON (offer_zones.offer_id = offers.id)
			  LEFT JOIN zones ON (zones.id = offer_zones.zone_id)
			  LEFT JOIN users ON (users.zone_id = zones.id)
			  LEFT JOIN users as creator on (creator.id = offers.created_by::INT)
			  where users.id='.$id.' AND offers.deleted_by IS NULL AND users.deleted_by IS NULL AND  (DATE(end_date) - DATE(CURRENT_DATE)) >= 0

			  group by offers.id, offers.title, offers.image, creator.type');

		$all_offers = array_merge($partner_offers, $other_offers);
		return response()->json(["partner_info"=>$partner_info,"brands"=>$partner_brands, "offers"=>$all_offers, "status"=>1], 201);
	}

 public function importExcel(Request $request)
	{
		$zones = Zones::lists('id','name');
		$brands = Brands::lists('id','name');
		$count=0;
		$current_time=Carbon::now()->toDayDateTimeString();
		if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {})->get();

			//print_r($data);die;


			if(!empty($data) && $data->count()){
				foreach ($data->toArray() as $key => $value) {
						if(!empty($value)){
							$zone_id=$zones[$value['zone']];		
							$insert = ['name' => $value['name'], 'email' => $value['email'],'city' => $value['city'],'address' => $value['address'],'phone' => $value['phone'],'zone_id' => $zone_id,'type'=>'partner','created_at'=>$current_time];
							if(!empty($insert)){
								DB::beginTransaction();
								try{

										$partner_id=Partners::insertGetId($insert);
										$insert_partner=[
											'user_id'=>$partner_id,
											'brand_id'=>$brands[$value['brand']],
											'created_at'=>$current_time
										];
										Partner_brands::insert($insert_partner);
								
										DB::commit();
										$count++;
										$request->session()->flash('success-message', $count.' Succesfully Inserted');	
									}catch (\Exception $e) {
										DB::rollback();
										
										$request->session()->flash('error-message', 'Insert Failed');
									// something went wrong
								}
								

							}
						
						}

				}
				
				return back()->with('success-message'," Data inserted successfully.");
			}
		}
		return back()->with('error-message','Please choose valid excel file.');

	}

}
