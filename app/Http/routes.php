<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');

//Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('/',     'HomeController@index');
Route::get('admin/home/', 'HomeController@index');

// Route::post('users/store', 'UserController@store');

Route::group(['prefix' => 'admin','middleware' => ['auth']], function(){
	Route::get('events/get_all', 'EventsController@get_all');
	Route::resource('events', 'EventsController');

	Route::get('offers/get_all', 'OffersController@get_all');
	Route::resource('offers', 'OffersController');

	Route::resource('redeemeds', 'RedeemedsController');	

	Route::get('changepassword',['as' => 'admin.changepassword','uses' =>'UserController@changepassword']);
    Route::post('updatepassword',['as' => 'admin.updatepassword','uses' =>'UserController@updatepassword']);
});

Route::group(['prefix' => 'admin','middleware' => ['auth','admin']], function(){

	Route::resource('user', 'UserController');
	Route::get('brands/get_all','BrandsController@get_all');
	Route::resource('brands','BrandsController');

	Route::get('zones/get_all', 'ZonesController@get_all');
	Route::resource('zones', 'ZonesController');

	Route::get('partners/get_all', 'PartnersController@get_all');
	Route::resource('partners', 'PartnersController');


	Route::post('importExcel','PartnersController@importExcel');

	Route::post('reset_password',['as' => 'admin.partners.reset_password','uses' =>'PartnersController@reset_password']);
});

Route::group(['prefix'=>'admin'], function(){
Route::post('newpassword',['as' => 'admin.newpassword','uses' =>'UserController@newpassword']);
});

Route::group(['prefix'=>'v1'], function(){
	Route::post('users/store', 'UserController@store');
	Route::get('zones/list','ZonesController@nearbyzones');
	Route::get('partners/get_partners/{id}', 'PartnersController@get_partners');
	Route::get('partners/details/{id}','PartnersController@details');
	Route::get('offers/view/{offer_id}', 'OffersController@view_offer');
	Route::get('events/list','EventsController@nearbyevents');
	Route::get('events/view/{id}', 'EventsController@view_event');

	Route::get('events/notification', 'EventsController@event_notify');
	
	Route::post('redeemeds/redeem_offer', 'RedeemedsController@redeem_offer');
	Route::get('redeemeds/user_redeemed', 'RedeemedsController@user_redeem_offer');
	Route::get('zones/search', 'ZonesController@search_zones');
	Route::get('events/search', 'EventsController@search_events');
	Route::get('offers/list', 'OffersController@nearbyofffers');
	Route::get('offers/birthdayoffers/{user_id}', 'OffersController@birthday_offers');
	Route::get('offers/search', 'OffersController@search_offers');
	Route::get('notification', 'UserController@notify');	
	Route::post('users/location', 'UserController@user_location');	
	Route::post('users/favourite', 'UserController@user_favourites');
	Route::post('users/favouritelist', 'UserController@user_favourite_list');
});
