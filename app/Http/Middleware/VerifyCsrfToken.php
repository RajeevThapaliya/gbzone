<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */

	private $openRoutes = ['v1/users/store','v1/redeemeds/redeem_offer','v1/users/location','v1/users/favourite','v1/users/favouritelist'];

	public function handle($request, Closure $next)
	{
		foreach($this->openRoutes as $route) {

			if ($request->is($route)) {
			return $next($request);
			}
		}
		return parent::handle($request, $next);
	}

}
