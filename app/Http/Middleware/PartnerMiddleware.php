<?php namespace App\Http\Middleware;
use Closure;
use App;
use Auth;

class PartnerMiddleware
{   
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->type != 'partner') {
            return redirect('admin/home/');
        }

        return $next($request);
    }
}