<?php namespace App\Http\Middleware;
use Closure;
use App;
use Auth;

class MemberMiddleware
{   
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->type != 'member') {
            return redirect('admin/home/');
        }

        return $next($request);
    }
}