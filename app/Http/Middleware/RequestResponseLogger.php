<?php namespace App\Http\Middleware;

// use Closure;
// use Log;

// class RequestResponseLogger {

// 	/**
// 	 * Handle an incoming request.
// 	 *
// 	 * @param  \Illuminate\Http\Request  $request
// 	 * @param  \Closure  $next
// 	 * @return mixed
// 	 */
// 	public function handle($request, Closure $next)
// 	{
// 		return $next($request);
// 	}
// 	public function terminate($request, $response)
//     {
//         Log::info('requests', [
//             'request' => $request->all(),
//             'response' => $response
//         ]);
//     }

// }



use Closure;  
use Illuminate\Contracts\Routing\TerminableMiddleware;  
use Illuminate\Support\Facades\Log;

class RequestResponseLogger implements TerminableMiddleware {

    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        Log::info('app.requests', ['request' => $request->all(), 'response' => $response]);
    }

}
